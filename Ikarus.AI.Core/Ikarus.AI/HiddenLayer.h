/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.19                                             */
/* Description : Output layer for neurons that returns the output data  */
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"
#include "Layer.h"

namespace IK_AI_NAMESPACE
{
	/// @brief The hidden layer is between the input and output layer and stores the magic "black box" of neurons.
	/// You will find any type of neuron in here.
	class HiddenLayer final : public Layer
	{
		MEMORY(HiddenLayer);
	public:
		using Layer::Layer;
		using Layer::operator=;
	};
}