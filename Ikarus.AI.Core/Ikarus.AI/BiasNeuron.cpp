/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.20                                             */
/* Description : Bias neuron always returning 1                         */
/*                                                                      */
/************************************************************************/
#include "stdafx.h"
#include "BiasNeuron.h"
#include "NeuronFactory.h"
using namespace IK_AI_NAMESPACE;
IK_REGISTER_NEURON(BiasNeuron);

void BiasNeuron::run()
{
	setOutput(1);
}

Constants::PrecisionType BiasNeuron::function(const Constants::PrecisionType)
{
	return 1;
}

Constants::PrecisionType BiasNeuron::derivative(const Constants::PrecisionType)
{
	return 0;
}
