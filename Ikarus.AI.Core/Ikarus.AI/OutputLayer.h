/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.19                                             */
/* Description : Output layer for neurons that returns the output data  */
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"
#include "Layer.h"

namespace IK_AI_NAMESPACE
{
	/// @brief The output layer is the last layer of a neural network and stores the output neurons.
	/// The output neurons are the last neurons that returns their output data. They usually only map
	/// its input values between -1 and 1 or 0 and 1 (whatever you need the output to be)
	/// To achieve this you would use <br>
	/// the logistic sigmoid neuron (mapping: 0 - 1), <br>
	/// the tanh neuron (mapping: -1 - 1) or <br>
	/// the binary neuron (mapping: 0 or 1 [no inbetween])
	class OutputLayer final : public Layer
	{
		MEMORY(OutputLayer);
		
	public:
		using Layer::Layer;
		using Layer::operator=;
	};
}