/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.21                                             */
/* Description : Class for creating and training neural networks        */
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"
#include "InputLayer.h"
#include "HiddenLayer.h"
#include "OutputLayer.h"
#include "Neuron.h"
#include "Genome.h"
#include "NeuronFactory.h"

#include "Ikarus.Common/Serializable.h"

namespace IK_AI_NAMESPACE
{
	
	/// @brief ld stands for layer definition and holds the number of neurons + the neuron type	
	template<typename ... T>
	std::tuple<T...> layer(T ... args)
	{
		return std::tuple<T...> { args... };
	}
	
	/// @brief Class providing bundled access to layers and its neurons
	class IK_AI_EXPORT NeuralNetwork : public IK_Common_NAMESPACE::Serializable
	{
		MEMORY(NeuralNetwork);

	public: /*typedefs*/
		using pt = Constants::PrecisionType;

	public: /*ctors and creators*/

		template<typename ...N>
		static NeuralNetwork Create(N... neurons);

		template<typename ...N>
		static NeuralNetwork Create(std::tuple<N, QString> ... neurons);

		template<typename ...N>
		static NeuralNetwork Create(std::tuple<N, const char*> ... neurons);

		NeuralNetwork() = default;

	public: /*getters and setters*/
		/// @brief Returns the input layer (first layer) of this neural network
		const InputLayer& inputLayer() const;
		
		/// @brief Returns the hidden layers (black box) of this neural network
		const std::vector<HiddenLayer>& hiddenLayers() const;
		
		/// @brief Returns the output layer (last layer) of this neural network
		const OutputLayer& outputLayer() const;

		/// @brief Returns the number of layers this neural network consists of (including input and output layer)
		uint64_t layers() const;

	public: /*behavioural*/

		/// @brief Adds a new hidden layer at the index position (index including input and output layer)
		/// @param index Where to add the layer
		void addHiddenLayer(const uint64_t index);

		/// @brief Adds the given neuron to the given layer index
		/// @param neuron The neuron to add to the neural network
		/// @param layer Where to add the neuron (0 = input layer, ... = hidden layer, last = output layer)
		Neuron::SPtr addNeuron(Neuron::UPtr neuron, uint64_t layer);

		/// @brief Connects all neurons of a layer with all neurons of the next layer
		void fullyConnect();

		/// @brief Runs this neural network with the given input and returns the NNs output
		/// @note This function is not responsible for error checking (number of inputs matching)
		/// @param input The input values
		/// @return The output values
		std::vector<pt> run(const std::vector<pt>& input);
		
		/// @brief Returns the number of neurons this neural network stores
		uint64_t neuronSize() const;

	public: /*IO*/
		/// @copydoc Serializable::serialize
		QJsonObject serialize() const override;
		
		/// @copydoc Serializable::deserialize
		bool deserialize(const QJsonObject& object) override;

	private:
		/// @brief Reconnects the neurons after a deserialization
		void reconnectNeurons();

		/// @brief Reconnects this genome to its appropriate neurons
		/// @param genome The genome to reconnect
		void reconnectByGenome(const Genome::SPtr& genome);

		/// @brief Returns a neuron with the given innovation number or nullptr if not found
		/// @param innovation The innovation to search for
		Neuron::SPtr neuronByInnovation(uint64_t innovation);
		
		InputLayer m_inputLayer;
		OutputLayer m_outputLayer;
		std::vector<HiddenLayer> m_hiddenLayers = {};
		uint64_t m_innovation = 0;
	};

	template <typename ... N>
	NeuralNetwork NeuralNetwork::Create(N... neurons)
	{
		static_assert(true && (... && std::is_convertible_v<N, uint64_t>), "Integers needed!");
		static_assert(sizeof...(N) >= 2, "At least input and output layer needed");
		std::vector<uint64_t> list = { static_cast<uint64_t>(neurons)... };

		NeuralNetwork network;
					
		for (uint64_t i = 0; i < list[0]; ++i)
			network.m_inputLayer.addNeuron(NeuronFactory::Create("Simple"))->setInnovation(network.m_innovation++);
		network.m_inputLayer.addNeuron(NeuronFactory::Create("Bias"))->setInnovation(network.m_innovation++);
			  		
		for (uint64_t n = 1; n < list.size() - 1; ++n)
		{
			auto& layer = network.m_hiddenLayers.emplace_back();
			for (uint64_t i = 0; i < list[n]; ++i)
				layer.addNeuron(NeuronFactory::Create("TanH"))->setInnovation(network.m_innovation++);
		}

		for (uint64_t i = 0; i < list[list.size() - 1]; ++i)
			network.m_outputLayer.addNeuron(NeuronFactory::Create("TanH"))->setInnovation(network.m_innovation++);
		
		return network;
	}

	template <typename ... N>
	NeuralNetwork NeuralNetwork::Create(std::tuple<N, QString>... neurons)
	{
		static_assert(true && (... && std::is_convertible_v<N, uint64_t>), "Integers needed!");
		static_assert(sizeof...(N) >= 2, "At least input and output layer needed");
		std::vector<std::tuple<uint64_t, QString>> list = { std::tuple<uint64_t, QString> { std::get<0>(neurons), std::get<1>(neurons) } ... };

		NeuralNetwork network;
		
		for (uint64_t i = 0; i < std::get<0>(list[0]); ++i)
			network.m_inputLayer.addNeuron(NeuronFactory::Create(std::get<1>(list[0])))->setInnovation(network.m_innovation++);
		network.m_inputLayer.addNeuron(NeuronFactory::Create("Bias"))->setInnovation(network.m_innovation++);
			  		
		for (uint64_t n = 1; n < list.size() - 1; ++n)
		{
			auto& layer = network.m_hiddenLayers.emplace_back();
			for (uint64_t i = 0; i < std::get<0>(list[n]); ++i)
				layer.addNeuron(NeuronFactory::Create(std::get<1>(list[n])))->setInnovation(network.m_innovation++);
		}

		for (uint64_t i = 0; i < std::get<0>(list[list.size() - 1]); ++i)
			network.m_outputLayer.addNeuron(NeuronFactory::Create(std::get<1>(list[list.size() - 1])))->setInnovation(network.m_innovation++);
		
		return network;
	}

	template <typename ... N>
	NeuralNetwork NeuralNetwork::Create(std::tuple<N, const char*>... neurons)
	{
		return Create(std::tuple<N, QString>(std::get<0>(neurons), std::get<1>(neurons))...);
	}
}
