#pragma once

#include "AIExport.h"

#include "Ikarus.Common/stdafx.h"
#include "Ikarus.Communication/stdafx.h"
#include "Ikarus.OpenCL/stdafx.h"

#define MEMORY(CLS) \
public: \
using UPtr = std::unique_ptr<CLS>; \
using SPtr = std::shared_ptr<CLS>; \
private:

