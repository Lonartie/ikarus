/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.19                                             */
/* Description : Class for collection of neurons as a layer             */
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"

#include "Genome.h"
#include "Neuron.h"

#include "Ikarus.Common/Serializable.h"

namespace IK_AI_NAMESPACE
{
	/// @brief This layer stores a list of neurons in a row.
	/// A layer in context of a neural network is a row of neurons. Neurons may only connect with other neurons (through genomes)
	/// that are on their right side (further to the output layer). This prevents recursive connections
	/// 
	/// @code
	/// O  -  O  -  O
	/// O  \- O  \  O 
	/// O  -  O  /- O
	/// L1 -  L2  - L3
	/// 
	/// L1 = Input layer
	/// L2 = Hidden layer # 1
	/// L3 = Output layer
	/// @endcode
	class IK_AI_EXPORT Layer : public IK_Common_NAMESPACE::Serializable
	{
		MEMORY(Layer);
		
	public:
		Layer(const Layer&) = delete;
		Layer& operator=(const Layer&) = delete;
		
		Layer() = default;
		Layer(Layer&&) = default;
		Layer& operator=(Layer&&) = default;
		~Layer() = default;
		
		/// @brief Returns the list of neurons stored in this layer
		const std::vector<Neuron::SPtr>& neurons() const;

		/// @brief Adds the given neuron to the internal list of neurons this layer stores
		/// @param neuron The neuron to add to the list
		Neuron::SPtr addNeuron(Neuron::SPtr neuron);

		/// @brief Returns the list of genomes stored in this layer.
		/// All neurons also hold their output genomes.
		const std::vector<Genome::SPtr>& genomes() const;
		std::vector<Genome::SPtr>& genomes();

		/// @brief Adds the given genome to the internal list of genomes this layer stores
		/// @param genome The genome to add to the list
		Genome::SPtr addGenome(Genome::SPtr genome);

		/// @brief Returns the neuron with the given \p innovation number
		/// @throws AccessViolation if this layer does not contain this neuron
		/// @param innovation The innovation number to search for
		/// @return The neuron that has this exact innovation number
		const Neuron::SPtr& neuron(uint64_t innovation) const;
		
		/// @brief Returns whether or not this layer stores a neuron with the given \p innovation number 
		/// @param innovation The innovation number to search for
		/// @return true when a neuron was found, false otherwise
		bool contains(uint64_t innovation) const;
		
		/// @brief Runs this layer by running each neuron. At this point the neurons should already have their input values
		virtual void run();

		/// @brief Returns the calculated output of this layer
		std::vector<Constants::PrecisionType> output() const;

	public: /*IO*/

		/// @copydoc Serializable::serializeTo
		virtual QJsonObject serialize() const override;
		
		/// @copydoc Serializable::deserializeFrom
		virtual bool deserialize(const QJsonObject& object) override;
		
	protected:
		std::vector<Neuron::SPtr> m_neurons;
		std::vector<Genome::SPtr> m_genomes;
	};
}
