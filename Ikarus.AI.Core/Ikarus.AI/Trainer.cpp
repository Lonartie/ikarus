/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.21                                             */
/* Description : Construct to train a neural network                    */
/*                                                                      */
/************************************************************************/
#include "stdafx.h"
#include "Trainer.h"
using namespace IK_AI_NAMESPACE;

Trainer::Trainer(NeuralNetwork& network)
	: m_network(network)
{}

const NeuralNetwork& Trainer::network() const
{
	return m_network;
}

NeuralNetwork& Trainer::network()
{
	return m_network;
}
