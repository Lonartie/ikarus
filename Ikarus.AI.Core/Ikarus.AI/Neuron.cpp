/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.19                                             */
/* Description : Neuron class for AI representing human neurons         */
/*                                                                      */
/************************************************************************/
#include "stdafx.h"
#include "Neuron.h"
#include "Ikarus.Common/Random.h"
using namespace IK_AI_NAMESPACE;
using namespace IK_Common_NAMESPACE;

Neuron::Neuron(const uint64_t innovation)
	: m_innovation(innovation)
{}

uint64_t Neuron::innovation() const
{
	return m_innovation;
}

void Neuron::setInnovation(const uint64_t innovation)
{
	m_innovation = innovation;
}

Constants::PrecisionType Neuron::output() const
{
	return m_output;
}

void Neuron::addInput(const Constants::PrecisionType input)
{
	m_input += input;
}

void Neuron::clearInputs()
{
	m_input = 0;
}

void Neuron::addInputGenome(const Genome::SPtr& genome)
{
	m_inputGenomes.push_back(genome);
}

Genome::SPtr Neuron::Connect(const Neuron::SPtr& a, const Neuron::SPtr& b, const Constants::PrecisionType minWheight, const Constants::PrecisionType maxWheight)
{
	return Connect(a, b, Random<Constants::PrecisionType>(minWheight, maxWheight));
}

Genome::SPtr Neuron::Connect(const Neuron::SPtr& a, const Neuron::SPtr& b, const Constants::PrecisionType wheight)
{
	auto genome = std::make_shared<Genome>();
	genome->setWeight(wheight);
	a->m_outputGenomes.push_back(genome);
	b->m_inputGenomes.push_back(genome);
	genome->setInputNeuron(a);
	genome->setOutputNeuron(b);
	a->m_outputGenomeIDs.push_back(genome->ID());
	b->m_inputGenomeIDs.push_back(genome->ID());
	return genome;
}

void Neuron::addOutputGenome(const Genome::SPtr& genome)
{
	m_outputGenomes.push_back(genome);
}

Constants::PrecisionType Neuron::function(const Constants::PrecisionType x)
{
	return 0;
}

Constants::PrecisionType Neuron::derivative(const Constants::PrecisionType x)
{
	return 0;
}

QJsonObject Neuron::serialize() const
{
	QJsonObject output;
	output.insert("Innovation", QString::number(m_innovation));
	output.insert("LastOutput", m_output);
	output.insert("LastInput", m_input);

	QJsonArray inGenomes;
	for (auto& id : m_inputGenomeIDs)
		inGenomes.push_back(QString::number(id));
	output.insert("InputGenomeIDs", inGenomes);

	QJsonArray outGenomes;
	for (auto& id : m_outputGenomeIDs)
		outGenomes.push_back(QString::number(id));
	output.insert("OutputGenomeIDs", outGenomes);

	return output;
}

bool Neuron::deserialize(const QJsonObject& object)
{
	m_innovation = object["Innovation"].toString().toULongLong();
	m_output = object["LastOutput"].toVariant().value<Constants::PrecisionType>();
	m_input = object["LastInput"].toVariant().value<Constants::PrecisionType>();

	m_inputGenomeIDs.clear();
	for (const auto& item : object["InputGenomeIDs"].toArray())
		m_inputGenomeIDs.push_back(item.toVariant().value<Constants::PrecisionType>());
	
	m_outputGenomeIDs.clear();
	for (const auto& item : object["OutputGenomeIDs"].toArray())
		m_outputGenomeIDs.push_back(item.toVariant().value<Constants::PrecisionType>());
	
	return true;
}

Constants::PrecisionType Neuron::input() const
{
	return m_input;
}

void Neuron::setOutput(const Constants::PrecisionType output)
{
	m_output = output;
	
	for(auto& genome : m_outputGenomes)
	{
		genome->setInput(output);
		genome->run();
	}

	clearInputs();
}

const std::vector<Genome::SPtr>& Neuron::outputGenomes() const
{
	return m_outputGenomes;
}

const std::vector<uint64_t>& Neuron::outputGenomeIDs() const
{
	return m_outputGenomeIDs;
}

const std::vector<Genome::SPtr>& Neuron::inputGenomes() const
{
	return m_inputGenomes;
}

const std::vector<uint64_t>& Neuron::inputGenomeIDs() const
{
	return m_inputGenomeIDs;
}



