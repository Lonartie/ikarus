/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.20                                             */
/* Description : Neuron class with tanh activation function (-1 to 1)   */
/*                                                                      */
/************************************************************************/
#include "stdafx.h"
#include "TanHNeuron.h"
#include "NeuronFactory.h"
using namespace IK_AI_NAMESPACE;
IK_REGISTER_NEURON(TanHNeuron);

void TanHNeuron::run()
{
	setOutput(function(input()));
}

Constants::PrecisionType TanHNeuron::function(const Constants::PrecisionType x)
{
	return std::tanh(x);
}

Constants::PrecisionType TanHNeuron::derivative(const Constants::PrecisionType x)
{
	const auto sh = 1.0 / std::cosh(x);
	return sh * sh;
}
