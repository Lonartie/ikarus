/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.20                                             */
/* Description : Neuron where output = input										*/
/*                                                                      */
/************************************************************************/
#include "stdafx.h"
#include "SimpleNeuron.h"
#include "NeuronFactory.h"
using namespace IK_AI_NAMESPACE;
IK_REGISTER_NEURON(SimpleNeuron);

void SimpleNeuron::run()
{
	setOutput(function(input()));
}

Constants::PrecisionType SimpleNeuron::function(const Constants::PrecisionType x)
{
	return x;
}

Constants::PrecisionType SimpleNeuron::derivative(const Constants::PrecisionType)
{
	return 1;
}
