/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.05.24                                             */
/* Description : Versioning file                                        */
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"

namespace IK_AI_NAMESPACE::Version
{
	extern IK_EXPORT(AI) const char* BuildDate;
	extern IK_EXPORT(AI) const char* LastCommitID;
	extern IK_EXPORT(AI) const char* BuildBranch;
	extern IK_EXPORT(AI) const char* AssemblyName;
}                                                                                                    