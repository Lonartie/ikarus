/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.20                                             */
/* Description : Neuron class with binary ac. function (0 or 1)			*/
/*                                                                      */
/************************************************************************/
#include "stdafx.h"
#include "BinaryNeuron.h"
#include "NeuronFactory.h"
using namespace IK_AI_NAMESPACE;
IK_REGISTER_NEURON(BinaryNeuron);

void BinaryNeuron::run()
{
	setOutput(function(input()));
}

Constants::PrecisionType BinaryNeuron::function(const Constants::PrecisionType x)
{
	return x >= 0
		? 1
		: 0;
}

Constants::PrecisionType BinaryNeuron::derivative(const Constants::PrecisionType)
{
	return 0;
}
