/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.25                                             */
/* Description : Gradient descent algorithms like error backpropagation */
/*                                                                      */
/************************************************************************/
#include "stdafx.h"
#include "GradientDescent.h"
#include "Maths.h"
#include "Ikarus.Common/Vector.h"
using namespace IK_AI_NAMESPACE;
using namespace IK_Common_NAMESPACE;

std::vector<Constants::PrecisionType> GradientDescent::ErrorBackPropagation(NeuralNetwork& network, std::vector<Constants::PrecisionType>& errorMap, const std::vector<Constants::PrecisionType>& outputErrors)
{
	const auto& hiddenLayers = network.hiddenLayers();
	const auto& outputNeurons = network.outputLayer().neurons();
	const auto& inputNeurons = network.inputLayer().neurons();
	std::fill(errorMap.begin(), errorMap.end(), 0);

	// foreach neuron in order (beginning fast output)
	for (uint64_t n = 0; n < outputNeurons.size(); ++n)
		errorMap[outputNeurons[n]->innovation()] = outputErrors[n];

	// foreach neuron in order (next is reversed hidden layer)
	for (int64_t i = hiddenLayers.size() - 1; i >= 0; --i)
		// foreach neuron in this layer
		for (const auto& neuron : hiddenLayers[i].neurons())
			errorMap[neuron->innovation()] = CalculateNeuronError(errorMap, neuron);

	// foreach neuron in order (last is input layer)
	for (const auto& neuron : inputNeurons)
		errorMap[neuron->innovation()] = CalculateNeuronError(errorMap, neuron);

	return errorMap;
}

void GradientDescent::AdjustWeights(NeuralNetwork& network, std::vector<Constants::PrecisionType>& errorMap, const Constants::PrecisionType learningRate)
{
	const auto& hiddenLayers = network.hiddenLayers();
	const auto& inputGenomes = network.inputLayer().genomes();

	// foreach neuron in order (next is reversed hidden layer)
	for (int64_t i = hiddenLayers.size() - 1; i >= 0; --i)
		// foreach neuron in this layer
		for (const auto& genome : hiddenLayers[i].genomes())
			genome->adjustWeight(CalculateWeightAdjustment(errorMap, genome) * learningRate);

	// foreach neuron in order (last is input layer)
	for (const auto& genome : inputGenomes)
		genome->adjustWeight(CalculateWeightAdjustment(errorMap, genome) * learningRate);
}

Constants::PrecisionType GradientDescent::CalculateNeuronError(std::vector<Constants::PrecisionType>& errorMap, const Neuron::SPtr& neuron)
{
	Constants::PrecisionType errorSum = 0;
	for (const auto& genome : neuron->outputGenomes())
	{
		const auto& errorOutputNeuron = errorMap[genome->outputNeuron()->innovation()];
		errorSum += errorOutputNeuron * genome->weight();
	}

	return errorSum / neuron->outputGenomes().size() * neuron->derivative(neuron->output());
}

Constants::PrecisionType GradientDescent::CalculateWeightAdjustment(std::vector<Constants::PrecisionType>& errorMap, const Genome::SPtr& genome)
{
	return genome->inputNeuron()->output() * errorMap[genome->outputNeuron()->innovation()];
}
