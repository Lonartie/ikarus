/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.25                                             */
/* Description : General Maths class for neural network calculations    */
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"
#include "Constants.h"

namespace IK_AI_NAMESPACE
{
	class IK_AI_EXPORT Maths
	{
	public:

		using Error = Constants::PrecisionType;
		using Errors = std::vector<Constants::PrecisionType>;
		using Cost = Constants::PrecisionType;
		using Costs = std::vector<Constants::PrecisionType>;

		using Outputs = std::vector<Constants::PrecisionType>;
		using Targets = std::vector<Constants::PrecisionType>;

		static Errors CalculateErrors(const Outputs& outputs, const Targets& targets);

		static Error SumErrors(const Errors& errors);

		static Cost SumCosts(const Costs& costs);
		
		static Cost CalculateCosts(const Errors& errors);

		static Cost CalculateCost(Constants::PrecisionType error);
		
	};
}
