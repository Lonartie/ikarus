/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.19                                             */
/* Description : Creates a neuron based on the given probabilities      */
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"
#include "Neuron.h"
#include "NeuronProbabilities.h"

namespace IK_AI_NAMESPACE
{
	/// @brief A Factory creating neurons based on the given probability settings Ikarus::AI::NeuronProbability.
	class IK_AI_EXPORT NeuronFactory final
	{
	public:

		/// @brief Typedef for functor creating a unique neuron pointer
		using NeuronCreator = std::function<Neuron::UPtr()>;

		/// @brief Registeres the given class as a neuron to be created by the factory
		/// @tparam T The class to be registered as a neuron
		/// @note T must be derived from base class neuron
		/// @return True all the time (for the register macro to function properly)
		template<typename T>
		static bool RegisterNeuron(const char* name);

		/// @brief Returns the number of registered neuron classes
		static unsigned RegisteredNeurons();

		/// @brief Returns the names of all registered neuron classes
		static const QStringList& RegisteredNeuronNames();

		/// @brief Creates a new neuron based on the given \p probabilities
		/// @param probabilities The probability for each type of neuron (ranges from 0 to 100 [percentages])
		/// @return The created neuron
		static Neuron::UPtr Create(const NeuronProbabilities& probabilities);
		
		/// @brief Creates a ner neuron with the given \p name initialized with the given \p innovation
		/// @param name The name of the neuron to create
		/// @return The created neuron
		static Neuron::UPtr Create(const QString& name);

		/// @brief Returns the neuron creator functions for classes that have been registered
		static const std::vector<NeuronCreator>& neuronCreators();

	private:

		static std::vector<NeuronCreator>& creatorFunctions();
		static std::vector<std::type_index>& creatorIndices();
		static QStringList& creatorNames();

	};

	template <typename T>
	bool NeuronFactory::RegisterNeuron(const char* name)
	{
		static_assert(std::is_base_of_v<Neuron, T>, "T must be derived from neuron");
		
		auto creator = []()
		{
			return std::make_unique<T>();
		};

		creatorFunctions().push_back(std::move(creator));
		creatorIndices().push_back(typeid(T));
		creatorNames() << (QString(name).toLower().replace("neuron", ""));
		return true;
	}
}

#define IK_REGISTER_NEURON(CLS) \
namespace \
{ \
	static auto IK_RANDOM_NAME = IK_AI_NAMESPACE::NeuronFactory::RegisterNeuron<CLS>(STRING(CLS)); \
}


