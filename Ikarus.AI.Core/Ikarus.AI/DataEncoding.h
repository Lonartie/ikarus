/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.26                                             */
/* Description : Data encoding to pass any type of data to a neural net */
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"
#include "Constants.h"

namespace IK_AI_NAMESPACE
{
	/// @brief Base class for data encoding an decoding for neural networks.
	/// @brief This way you are able to input one of many elements no matter of which type into a neural network.
	/// @brief The encoded list size and the decode list size must be equal
	/// @note If your data set grows or shrinks in time, this algorithm is 100% useless!
	/// @tparam T 
	template<typename T>
	class DataEncoding
	{
	public:
		virtual ~DataEncoding() = default;

		/// @brief Sets the data set to be encoded from and decoded to
		/// @param data The data set for translation
		virtual void setData(std::vector<T> data);

		/// @brief Encodes a given object T into a list of inputs for a neural network
		/// @param in The object to be encoded
		/// @return The encoded list of neural network inputs
		virtual std::vector<Constants::PrecisionType> encode(const T& in) const = 0;
		
		/// @brief Decodes a given list of inputs from a neural network to the appropriate object T
		/// @param out The neural networks output to be decoded
		/// @return The decoded value based on the data set
		virtual const T& decode(const std::vector<Constants::PrecisionType>& out) = 0;

		/// @brief Analyzes the input data to build a translation map
		/// @return The minimum input / output neurons needed to encode and decode
		virtual uint64_t analyze() const = 0;

	protected:

		/// @brief Returns the data set
		const std::vector<T>& data() const;
		
	private:

		std::vector<T> m_data;
		
	};

	template <typename T>
	void DataEncoding<T>::setData(std::vector<T> data)
	{
		m_data = std::move(data);
	}

	template <typename T>
	const std::vector<T>& DataEncoding<T>::data() const
	{
		return m_data;
	}
}
