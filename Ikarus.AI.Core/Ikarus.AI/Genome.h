/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.19                                             */
/* Description : Genome class representing human genomes (connections)  */
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"
#include "Constants.h"

#include "Ikarus.Common/Serializable.h"

namespace IK_AI_NAMESPACE
{
	class Neuron;
	
	/// @brief The genome is the part of a neural network that connects two neurons.
	/// While doing this it receives the output value from its input neuron, multiplies it
	/// with a weight and passes the result to its output neuron to be further processed.
	class IK_AI_EXPORT Genome final : public IK_Common_NAMESPACE::Serializable
	{
		MEMORY(Genome);

	private:

		static uint64_t UniqueID();
		
	public:

		Genome();

		/// @brief Returns the unique genome ID for identification 
		uint64_t ID() const;

		/// @brief Returns whether or not this genome is active
		/// @note Only when this genome is active, Genome::run does something, otherwise its just returning
		bool active() const;
		
		/// @brief Sets whether or not this genome should be active
		/// @param active Whether or not this genome should be active
		void setActive(const bool active);
		
		/// @brief Returns this genomes weight
		Constants::PrecisionType weight() const;

		/// @brief Sets this genomes weight
		/// @param weight The new weight for this genome
		void setWeight(const Constants::PrecisionType weight);

		/// @brief Adds the adjustment to this genomes weight
		/// @param adjustment The value to add to this genomes weight
		void adjustWeight(const Constants::PrecisionType adjustment);
		
		/// @brief Returns the last calculated output value
		Constants::PrecisionType output() const;
		
		/// @brief Returns the genomes input value
		Constants::PrecisionType input() const;
		
		/// @brief Sets the genomes \p input value to be passed to another neuron
		/// @param input The input value
		void setInput(Constants::PrecisionType input);

		/// @brief Returns the genomes input neuron
		const std::shared_ptr<Neuron>& inputNeuron() const;
		
		/// @brief Sets the genomes input neuron
		/// @param inputNeuron The new input neuron for this genome
		void setInputNeuron(const std::shared_ptr<Neuron>& inputNeuron);
		
		/// @brief Returns the genomes output neuron
		const std::shared_ptr<Neuron>& outputNeuron() const;
		
		/// @brief Sets the genomes output neuron
		/// @param outputNeuron The new output neuron for this genome
		void setOutputNeuron(const std::shared_ptr<Neuron>& outputNeuron);
		
		/// @brief Runs this genome. Means that it calculates the output value by multiplying
		/// its input value with its weight and passes this value to its output neuron
		void run();

		/// @brief Returns the input neurons innovation number
		uint64_t inputNeuronInnovation() const;
		
		/// @brief Returns the output neurons innovation number
		uint64_t outputNeuronInnovation() const;

	public: /*IO*/

		/// @copydoc Serializable::serializeTo
		virtual QJsonObject serialize() const override;
		
		/// @copydoc Serializable::deserializeFrom
		virtual bool deserialize(const QJsonObject& object) override;

	private:
		bool m_active = true;
		uint64_t m_ID = 0;
		Constants::PrecisionType m_weight = 0;
		Constants::PrecisionType m_input = 0;
		Constants::PrecisionType m_output = 0;
		std::shared_ptr<Neuron> m_inputNeuron = nullptr;
		std::shared_ptr<Neuron> m_outputNeuron = nullptr;
		uint64_t m_inputNeuronInnovation = 0;
		uint64_t m_outputNeuronInnovation = 0;
	};
}
