/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.21                                             */
/* Description : Construct to train a neural network                    */
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"
#include "NeuralNetwork.h"

namespace IK_AI_NAMESPACE
{
	/// @brief Trains a neural network. <br>
	/// There are many different types a neural network can be trained. <br>
	/// They can be put in two categories. <br>
	/// @li Supervised: You have to know the real solution and tell the neural network how wrong its solution is
	/// @li Unsupervised: You don't know the best solution, but you must tell the neural network at least how good its solution is (score)
	/// These categories may contain multiple training mechanisms
	class IK_AI_EXPORT Trainer
	{
	public:
		Trainer(NeuralNetwork& network);
		virtual ~Trainer() = default;

		/// @brief Returns the neural network that is trained by this class 
		const NeuralNetwork& network() const;
		
		/// @brief Returns the neural network that is trained by this class 
		NeuralNetwork& network();
		
	private:
		NeuralNetwork& m_network;
	};
}