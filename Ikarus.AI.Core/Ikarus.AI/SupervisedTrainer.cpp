/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.21                                             */
/* Description : Supervised trainer for neural networks                 */
/*                                                                      */
/************************************************************************/
#include "stdafx.h"
#include "SupervisedTrainer.h"
using namespace IK_AI_NAMESPACE;

float SupervisedTrainer::accuracy(const TrainingTable& table)
{
	float accuracy = 0;
	for (const auto& set : table.Sets)
	{
		auto result = network().run(set.Inputs);
		for (uint64_t i = 0; i < result.size(); i++)
		{
			// gaussian distribution to make 0 error to 100% and higher/lower values towards 0% per value
			const float sh = 1.0 / std::cosh(std::abs(set.Targets[i] - result[i]));
			accuracy += sh * sh / result.size();
		}
	}
	return 100.0f * accuracy / table.Sets.size();
}
