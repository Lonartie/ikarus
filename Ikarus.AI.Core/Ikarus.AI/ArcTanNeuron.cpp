/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.20                                             */
/* Description : Neuron class with arctan ac. function (~-1.57 to ~1.57)*/
/*                                                                      */
/************************************************************************/
#include "stdafx.h"
#include "ArcTanNeuron.h"
#include "NeuronFactory.h"
using namespace IK_AI_NAMESPACE;
IK_REGISTER_NEURON(ArcTanNeuron);

void ArcTanNeuron::run()
{
	setOutput(function(input()));
}

Constants::PrecisionType ArcTanNeuron::function(const Constants::PrecisionType x)
{
	return std::atan(x);
}

Constants::PrecisionType ArcTanNeuron::derivative(const Constants::PrecisionType x)
{
	return 1.0 / (1.0 + x * x);
}

