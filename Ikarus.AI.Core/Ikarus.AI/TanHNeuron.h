/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.20                                             */
/* Description : Neuron class with tanh activation function (-1 to 1)   */
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"
#include "Neuron.h"

namespace IK_AI_NAMESPACE
{
	/// @brief Neuron with tanh activation function
	/// @image html tanh.png width=75%
	class IK_AI_EXPORT TanHNeuron final : public Neuron
	{
		NEURON(TanHNeuron);
		
	public:
		using Neuron::Neuron;
		
		/// @copydoc Neuron::run
		void run() override;
		
		/// @copydoc Neuron::function
		Constants::PrecisionType function(const Constants::PrecisionType x) override;
		
		/// @copydoc Neuron::derivative
		Constants::PrecisionType derivative(const Constants::PrecisionType x) override;
	};
}
