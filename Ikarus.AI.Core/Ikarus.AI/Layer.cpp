/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.19                                             */
/* Description : Class for collection of neurons as a layer             */
/*                                                                      */
/************************************************************************/
#include "stdafx.h"
#include "Layer.h"

#include "NeuronFactory.h"
using namespace IK_AI_NAMESPACE;

const std::vector<Neuron::SPtr>& Layer::neurons() const
{
	return m_neurons;
}

const std::vector<Genome::SPtr>& Layer::genomes() const
{
	return m_genomes;
}

std::vector<Genome::SPtr>& Layer::genomes()
{
	return m_genomes;
}

Genome::SPtr Layer::addGenome(Genome::SPtr genome)
{
	m_genomes.push_back(genome);
	return genome;
}

const Neuron::SPtr& Layer::neuron(uint64_t innovation) const
{
	const auto iter = std::find_if(m_neurons.begin(), m_neurons.end(), [innovation](const auto& neuron)
	{
		return neuron->innovation() == innovation;
	});

	return *iter;
}

bool Layer::contains(uint64_t innovation) const
{
	const auto iter = std::find_if(m_neurons.begin(), m_neurons.end(), [innovation](const auto& neuron)
	{
		return neuron->innovation() == innovation;
	});

	return iter != m_neurons.end();
}

Neuron::SPtr Layer::addNeuron(Neuron::SPtr neuron)
{
	m_neurons.push_back(neuron);
	return neuron;
}

void Layer::run()
{
	for (const auto& neuron : m_neurons)
	{
		neuron->run();
	}
}

std::vector<Constants::PrecisionType> Layer::output() const
{
	std::vector<Constants::PrecisionType> result;
	result.resize(m_neurons.size());
	
	for(uint64_t i = 0; i < m_neurons.size(); ++i)
		result[i] = m_neurons[i]->output();

	return result;
}

QJsonObject Layer::serialize() const
{
	QJsonObject output;

	QJsonArray neuronsJson;
	for (const auto& neuron: m_neurons)
	{
		QJsonObject neuronJson;
		neuronJson.insert("Type", neuron->type());
		neuronJson.insert("Data", neuron->serialize());
		neuronsJson.push_back(neuronJson);
	}
	output.insert("Neurons", neuronsJson);
	
	QJsonArray genomesJson;
	for (const auto& genomes: m_genomes)
		genomesJson.push_back(genomes->serialize());
	output.insert("Genomes", genomesJson);
	
	return output;
}

bool Layer::deserialize(const QJsonObject& object)
{
	m_neurons.clear();
	for (auto& item : object["Neurons"].toArray())
	{
		auto neuronJson = item.toObject();
		auto type = neuronJson["Type"].toString();
		auto& neuron = m_neurons.emplace_back(NeuronFactory::Create(type));
		
		if(!neuron->deserialize(neuronJson["Data"].toObject()))
			return false;
	}

	m_genomes.clear();
	for (auto& item : object["Genomes"].toArray())
	{
		auto& genome = m_genomes.emplace_back(std::make_unique<Genome>());
		
		if(!genome->deserialize(item.toObject()))
			return false;
	}

	return true;
}
