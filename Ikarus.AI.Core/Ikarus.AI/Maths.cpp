/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.25                                             */
/* Description : General Maths class for neural network calculations    */
/*                                                                      */
/************************************************************************/
#include "stdafx.h"
#include "Maths.h"
using namespace IK_AI_NAMESPACE;

Maths::Errors Maths::CalculateErrors(const Outputs& outputs, const Targets& targets)
{
	Errors out;
	out.resize(outputs.size());
	for(uint64_t i = 0; i < outputs.size(); i++)
		out[i] = targets[i] - outputs[i];
	return out;
}

Maths::Error Maths::SumErrors(const Errors& errors)
{
	return std::accumulate(errors.begin(), errors.end(), Error(0), [](auto& a, auto& b) {
		return std::abs(a) + std::abs(b);
	}) / Error(errors.size());
}

Maths::Cost Maths::SumCosts(const Costs& costs)
{
	return std::accumulate(costs.begin(), costs.end(), Cost(0), [](auto& a, auto& b) {
		return a + b;
	}) / Cost(costs.size());
}

Maths::Cost Maths::CalculateCosts(const Errors& errors)
{
	return std::accumulate(errors.begin(), errors.end(), Cost(0), [](auto& a, auto& b) {
		return std::pow(a, 2) + std::pow(b, 2);
	}) / Cost(errors.size());
}

Maths::Cost Maths::CalculateCost(const Error error)
{
	return std::pow<Cost>(error, 2);
}
