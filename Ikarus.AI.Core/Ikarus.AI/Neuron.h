/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.19                                             */
/* Description : Neuron class representing human neurons                */
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"
#include "Genome.h"
#include "Constants.h"

#include "Ikarus.Common/Serializable.h"

namespace IK_AI_NAMESPACE
{
	/// @brief Base neuron class (is abstract).
	/// Possible implementations are:
	/// <li> tanh </li>
	/// <li> logistic sigmoid </li>
	/// <li> ReLU </li>
	/// <li> binary </li>
	/// <li> arctan </li>
	/// <li> bias </li>
	/// <li> simple </li>
	class IK_AI_EXPORT Neuron : public IK_Common_NAMESPACE::Serializable
	{
		MEMORY(Neuron);
		
	public:
		Neuron() = default;
		Neuron(uint64_t innovation);
		virtual ~Neuron() = default;

		/// @brief Connects this neuron with the given one by creating a new genome with random weight
		/// @param a The neuron to connect from
		/// @param b The neuron to connect to
		/// @param minWheight The minimum weight
		/// @param maxWheight The maximum weight
		static Genome::SPtr Connect(const Neuron::SPtr& a, const Neuron::SPtr& b, const Constants::PrecisionType minWheight, const Constants::PrecisionType maxWheight);

		/// @brief Connects this neuron with the given one by creating a new genome with random weight
		/// @param a The neuron to connect from
		/// @param b The neuron to connect to
		/// @param weight The weight for the new genome
		static Genome::SPtr Connect(const Neuron::SPtr& a, const Neuron::SPtr& b, const Constants::PrecisionType wheight);
		
		/// @brief Returns the neurons innovation number
		uint64_t innovation() const;

		/// @brief Sets the neurons innovation number
		/// @param innovation The new innovation number
		void setInnovation(const uint64_t innovation);
		
		/// @brief Returns the neurons last calculated output value
		Constants::PrecisionType output() const;
		
		/// @brief Returns the neurons input values
		Constants::PrecisionType input() const;

		/// @brief Returns the output genomes this neuron is connected with
		const std::vector<Genome::SPtr>& outputGenomes() const;

		/// @brief Returns the output genome IDs this neuron is connected with
		const std::vector<uint64_t>& outputGenomeIDs() const;

		/// @brief Returns the input genomes this neuron is connected with
		const std::vector<Genome::SPtr>& inputGenomes() const;

		/// @brief Returns the input genome IDs this neuron is connected with
		const std::vector<uint64_t>& inputGenomeIDs() const;

		/// @brief Adds the given \p input to the neurons input list
		/// @param input The input value to add
		void addInput(Constants::PrecisionType input);
		
		/// @brief Clears all input values
		void clearInputs();
		
		/// @brief Adds the given genome to the input genomes list
		/// @param genome The genome to add to the input genomes list 
		void addInputGenome(const Genome::SPtr& genome);
		
		/// @brief Adds the given genome to the output genomes list
		/// @param genome The genome to add to the output genomes list 
		void addOutputGenome(const Genome::SPtr& genome);

		/// @brief Defines the activation function used to calculate the output of the neuron
		/// @param x The input value
		virtual Constants::PrecisionType function(const Constants::PrecisionType x);
		
		/// @brief Defines the derivative of the activation function used to calculate the output of the neuron
		/// @param x The input value
		virtual Constants::PrecisionType derivative(const Constants::PrecisionType x);
		
		/// @brief Runs this neuron with its saved input values and stores its
		/// result in its output value
		virtual void run() = 0;

		/// @brief Returns the name / type of the neuron (basically for serialization)
		virtual QString type() const = 0;

	public: /*IO*/

		/// @copydoc Serializable::serialize
		virtual QJsonObject serialize() const override;
		
		/// @copydoc Serializable::deserialize
		virtual bool deserialize(const QJsonObject& object) override;

	protected:

		/// @brief Sets the neurons output value to \p output
		/// @param output The output value of this neuron
		void setOutput(Constants::PrecisionType output);
		
	private:
		std::vector<Genome::SPtr> m_outputGenomes = {};
		std::vector<Genome::SPtr> m_inputGenomes = {};
		std::vector<uint64_t> m_inputGenomeIDs = {};
		std::vector<uint64_t> m_outputGenomeIDs = {};
		Constants::PrecisionType m_input = 0;
		Constants::PrecisionType m_output = 0;
		uint64_t m_innovation = 0;
	};
}

#define NEURON(CLS) \
public: \
	virtual inline QString type() const override { return QString(STRING(CLS)).replace("neuron", "", Qt::CaseInsensitive); }; \
private: