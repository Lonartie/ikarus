/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.21                                             */
/* Description : Defines data needed for a supervised training session  */
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"
#include "Constants.h"

namespace IK_AI_NAMESPACE
{
	struct TrainingSet
	{
		/// @brief The input values for the neural network
		std::vector<Constants::PrecisionType> Inputs = {};

		/// @brief The desired output of the neural network
		std::vector<Constants::PrecisionType> Targets = {};
	};

	struct TrainingTable
	{
		/// @brief The list of all desired outcomes
		std::vector<TrainingSet> Sets = {};
	};

	struct SGDResult
	{
		/// @brief The list of errors between output and target values
		/// @note Errors can cancel each other out, be aware!
		std::vector<Constants::PrecisionType> Errors = {};

		/// @brief The combined value of all errors
		/// @note Errors can cancel each other out, be aware!
		Constants::PrecisionType Error = 0;

		/// @brief Similar to the error value, it tells you how good the neural network was trained
		/// @note This value has no cancellation effects like the Error value :)
		Constants::PrecisionType Cost = 0;

		/// @brief The numbers of iterations the training actually ran
		uint64_t IterationsRan = 0;

		/// @brief Whether or not the Cost goal has been reached or not
		bool CostGoalReached = false;
	};

	struct SGDSettings
	{
		/// @brief The maximum number of combined training sessions
		/// @note The actual max number of times the training will run is MaxIterations * TrainingSetCount
		uint64_t MaxIterations = 1000;

		/// @brief The number of items of a set to train in one iteration
		uint64_t Epochs = 50;

		/// @brief Whether or not to allow that training will result on randomly chosen sets
		bool ShuffleInputSets = true;

		/// @brief The training will end when the DesiredError value is reached (or MaxIterations)
		Constants::PrecisionType DesiredCost = Constants::PrecisionType(0.01);

		/// @brief The learning rate which will define the speed of the regression towards the optimum value
		/// @note Lower values lead to more accurate solutions while taking longer to process
		Constants::PrecisionType LearningRate = Constants::PrecisionType(0.1);

		/// @brief The callback function (if set to not nullptr) will be run everytime the training
		/// did one full loop so that you can see the learning process
		std::function<void(SGDResult)> Callback = nullptr;
	};
}