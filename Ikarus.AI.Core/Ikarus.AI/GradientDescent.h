/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.25                                             */
/* Description : Gradient descent algorithms like error backpropagation */
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"
#include "Constants.h"
#include "NeuralNetwork.h"
#include "Neuron.h"

namespace IK_AI_NAMESPACE
{
	class IK_AI_EXPORT GradientDescent
	{
	public:

		/// @brief Create the entire error map based on the input errors using backpropagation gradient descent
		/// @param network The network to backpropagate through
		/// @param errorMap An initialized error map (just for memory, should be filled with zeros)
		/// @param outputErrors The calculated errors for the output neurons
		/// @return The entire error map for each and every neuron
		static std::vector<Constants::PrecisionType> ErrorBackPropagation(NeuralNetwork& network, std::vector<Constants::PrecisionType>& errorMap, const std::vector<Constants::PrecisionType>& outputErrors);

		/// @brief Adjusts the genomes weight of the given network based on the pre calculated error map using a certain learning rate
		/// @param network The network to get the genomes from
		/// @param errorMap The pre calculated error map
		/// @param learningRate The learning rate (how big the weight steps should be)
		static void AdjustWeights(NeuralNetwork& network, std::vector<Constants::PrecisionType>& errorMap, Constants::PrecisionType learningRate);
				
	private:
		
		/// @brief Calculates the error for the given neuron
		/// @note The error for the output neurons must have been already calulcated
		/// @param errorMap The error map to find the already calculated neuron errors
		/// @param neuron The neuron to calculate the error for
		/// @return The calculated error
		static Constants::PrecisionType CalculateNeuronError(std::vector<Constants::PrecisionType>& errorMap, const Neuron::SPtr& neuron);

		/// @brief Calculates delta w (how much the weight should be nudged)
		/// @param errorMap The precalculated error map
		/// @param genome Which genome to calculate the delta for
		/// @return The delta w
		static Constants::PrecisionType CalculateWeightAdjustment(std::vector<Constants::PrecisionType>& errorMap, const Genome::SPtr& genome);
	};
}
