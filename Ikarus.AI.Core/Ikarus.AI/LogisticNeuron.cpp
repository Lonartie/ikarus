/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.20                                             */
/* Description : Neuron class with logistic ac. function (0 to 1)			*/
/*                                                                      */
/************************************************************************/
#include "stdafx.h"
#include "LogisticNeuron.h"
#include "NeuronFactory.h"
using namespace IK_AI_NAMESPACE;
IK_REGISTER_NEURON(LogisticNeuron);

void LogisticNeuron::run()
{
	setOutput(function(input()));
}

Constants::PrecisionType LogisticNeuron::function(const Constants::PrecisionType x)
{
	return 1.0 / (1.0 + std::exp(-x));
}

Constants::PrecisionType LogisticNeuron::derivative(const Constants::PrecisionType x)
{
	const auto sx = function(x);
	return sx * (1 - sx);
}
