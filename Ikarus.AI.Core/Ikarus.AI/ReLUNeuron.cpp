/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.20                                             */
/* Description : Neuron class with relu ac. function (0 to inf)			*/
/*                                                                      */
/************************************************************************/
#include "stdafx.h"
#include "ReLUNeuron.h"
#include "NeuronFactory.h"
using namespace IK_AI_NAMESPACE;
IK_REGISTER_NEURON(ReLUNeuron);

void ReLUNeuron::run()
{
	setOutput(function(input()));
}

Constants::PrecisionType ReLUNeuron::function(const Constants::PrecisionType x)
{
	return std::max<Constants::PrecisionType>(0, x);
}

Constants::PrecisionType ReLUNeuron::derivative(const Constants::PrecisionType x)
{
	return x >= 0
		? 1
		: 0;
}

