/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.20                                             */
/* Description : Bias neuron always returning 1                         */
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"
#include "Neuron.h"

namespace IK_AI_NAMESPACE
{
	/// @brief Neuron which outputs 1 every time
	/// @image html bias.png width=75%
	class IK_AI_EXPORT BiasNeuron final: public Neuron
	{
		NEURON(BiasNeuron);
		
	public:
		using Neuron::Neuron;

		/// @copydoc Neuron::run
		void run() override;
		
		/// @copydoc Neuron::function
		Constants::PrecisionType function(const Constants::PrecisionType) override;
		
		/// @copydoc Neuron::derivative
		Constants::PrecisionType derivative(const Constants::PrecisionType) override;
	};
}