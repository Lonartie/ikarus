/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.19                                             */
/* Description : Input layer for neurons that receive the input data    */
/*                                                                      */
/************************************************************************/
#include "stdafx.h"
#include "InputLayer.h"
using namespace IK_AI_NAMESPACE;

void InputLayer::run(const std::vector<Constants::PrecisionType>& input)
{
	auto niter = m_neurons.begin();
	auto iiter = input.begin();
	
	for (; iiter < input.end(); ++niter, ++iiter)
	{
		(*niter)->addInput(*iiter);
	}

	Layer::run();
}
