/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.22                                             */
/* Description : Stochastic gradient descent supervised nn trainer      */
/*                                                                      */
/************************************************************************/
#include "stdafx.h"
#include "SGDTrainer.h"
#include "GradientDescent.h"
#include "Maths.h"

#include "Ikarus.Common/Random.h"
#include "Ikarus.Common/Vector.h"

using namespace IK_AI_NAMESPACE;
using namespace IK_Common_NAMESPACE;

SGDResult SGDTrainer::train(const TrainingTable& table, const SGDSettings& settings)
{
	SGDResult output;
	m_errorMap.resize(network().neuronSize());
	Maths::Costs costs;
	costs.resize(settings.Epochs);
	SGDResult tr;

	for (uint64_t n = 0; n < settings.MaxIterations; ++n)
	{
		output.IterationsRan = n + 1;

		for (uint64_t i = 0; i < settings.Epochs; ++i)
		{
			const auto* currentSet = settings.ShuffleInputSets
				? &table.Sets[i % table.Sets.size()]
				: &Random(table.Sets);

			tr = train(*currentSet, settings.LearningRate);

			output.Errors = AddVectors(output.Errors, tr.Errors);
			costs[i] = tr.Cost;
		}
		
		output.Errors = DivideVector(output.Errors, table.Sets.size());
		output.Error = Maths::SumErrors(output.Errors);
		output.Cost = Maths::SumCosts(costs);
		output.CostGoalReached = output.Cost <= settings.DesiredCost;

		if (settings.Callback)
		{
			std::invoke(settings.Callback, output);
		}

		if (output.CostGoalReached)
		{
			output.CostGoalReached = true;
			return output;
		}
	}

	return output;
}

SGDResult SGDTrainer::train(const TrainingSet& set, const Constants::PrecisionType learningRate)
{
	SGDResult tr;

	auto& net = network();
	const auto prediction = net.run(set.Inputs);
	const auto lastErrorList = Maths::CalculateErrors(prediction, set.Targets);

	tr.Errors = lastErrorList;
	tr.Error = Maths::SumErrors(tr.Errors);
	tr.Cost = Maths::CalculateCosts(tr.Errors);

	GradientDescent::ErrorBackPropagation(net, m_errorMap, lastErrorList);
	GradientDescent::AdjustWeights(net, m_errorMap, learningRate);

	return tr;
}
