#include "stdafx.h"
#include "NeuralNetwork.h"
using namespace IK_AI_NAMESPACE;

const std::vector<HiddenLayer>& NeuralNetwork::hiddenLayers() const
{
	return m_hiddenLayers;
}

const InputLayer& NeuralNetwork::inputLayer() const
{
	return m_inputLayer;
}

const OutputLayer& NeuralNetwork::outputLayer() const
{
	return m_outputLayer;
}

uint64_t NeuralNetwork::layers() const
{
	return m_hiddenLayers.size() + 2; // + input + output layer (+2)
}

void NeuralNetwork::addHiddenLayer(const uint64_t index)
{
	if (m_hiddenLayers.empty())
		m_hiddenLayers.emplace_back();
	else
		m_hiddenLayers.emplace(m_hiddenLayers.begin() + index - 1);
}

Neuron::SPtr NeuralNetwork::addNeuron(Neuron::UPtr neuron, uint64_t layer)
{
	std::shared_ptr _neuron = std::move(neuron);
	_neuron->setInnovation(m_innovation++);

	if (layer < 0 || layer >= layers())
	{
		throw std::out_of_range("the given layer is out of bounds");
	}
	
	if (layer == 0)
	{
		m_inputLayer.addNeuron(_neuron);
		return _neuron;
	}

	if (layer == layers() - 1)
	{
		m_outputLayer.addNeuron(_neuron);
		return _neuron;
	}

	m_hiddenLayers[layer - 1].addNeuron(_neuron);
	return _neuron;
}

void NeuralNetwork::fullyConnect()
{
	bool nextIsLast = m_hiddenLayers.empty();
	Layer* firstLayer = &m_inputLayer;
	auto* firstList = &m_inputLayer.neurons();
	auto* nextList =  nextIsLast ? &m_outputLayer.neurons() : &m_hiddenLayers[0].neurons();
	uint64_t index = 1;

	while(true)
	{
		for(const auto& firstNeuron : *firstList)
		{
			for (const auto& nextNeuron : *nextList)
			{
				auto genome = Neuron::Connect(firstNeuron, nextNeuron, -2, 2);
				firstLayer->addGenome(genome);
			}
		}

		if(nextIsLast)
		{
			break;
		}

		nextIsLast = index >= m_hiddenLayers.size();
		firstList = nextList;
		nextList = nextIsLast ? &m_outputLayer.neurons() : &m_hiddenLayers[index].neurons();
		firstLayer = &m_hiddenLayers[index - 1];
		index++;
	}
}

std::vector<NeuralNetwork::pt> NeuralNetwork::run(const std::vector<pt>& input)
{	
	m_inputLayer.run(input);
	for (auto& layer : m_hiddenLayers)
		layer.run();
	m_outputLayer.run();
	
	return m_outputLayer.output();
}

uint64_t NeuralNetwork::neuronSize() const
{
	uint64_t size = m_inputLayer.neurons().size() + m_outputLayer.neurons().size();
	for (auto& layer : m_hiddenLayers) size += layer.neurons().size();
	return size;
}

QJsonObject NeuralNetwork::serialize() const
{
	QJsonObject output;
	
	output.insert("Innovation", QString::number(m_innovation));
	output.insert("InputLayer", m_inputLayer.serialize());

	QJsonArray layersJson;
	for (const auto& layer : m_hiddenLayers)
		layersJson.push_back(layer.serialize());

	output.insert("HiddenLayers", layersJson);	
	output.insert("OutputLayer", m_outputLayer.serialize());
	
	return output;
}

bool NeuralNetwork::deserialize(const QJsonObject& object)
{
	m_innovation = object["Innovation"].toString().toULongLong();
	m_inputLayer.deserialize(object["InputLayer"].toObject());

	m_hiddenLayers.clear();
	for (auto& item : object["HiddenLayers"].toArray())
	{
		auto& layer = m_hiddenLayers.emplace_back();		
		if(!layer.deserialize(item.toObject()))
			return false;
	}

	if(!m_outputLayer.deserialize(object["OutputLayer"].toObject()))
		return false;
	
	reconnectNeurons();
	return true;
}

void NeuralNetwork::reconnectNeurons()
{
	std::vector<Genome::SPtr> genomes;

	for(auto& genome : m_inputLayer.genomes())
		genomes.push_back(genome);

	for(auto& layer : m_hiddenLayers)
		for(auto& genome : layer.genomes())
			genomes.push_back(genome);
		
	for(auto& genome : m_outputLayer.genomes())
		genomes.push_back(genome);

	for (auto& genome : genomes)
		reconnectByGenome(genome);
}

void NeuralNetwork::reconnectByGenome(const Genome::SPtr& genome)
{
	const auto from = genome->inputNeuronInnovation();
	const auto to = genome->outputNeuronInnovation();

	auto fromNeuron = neuronByInnovation(from);
	auto toNeuron = neuronByInnovation(to);

	genome->setInputNeuron(fromNeuron);
	genome->setOutputNeuron(toNeuron);
	
	fromNeuron->addOutputGenome(genome);
	toNeuron->addInputGenome(genome);
}

Neuron::SPtr NeuralNetwork::neuronByInnovation(const uint64_t innovation)
{
	if (m_inputLayer.contains(innovation))
		return m_inputLayer.neuron(innovation);

	for (auto& layer : m_hiddenLayers)
		if (layer.contains(innovation))
			return layer.neuron(innovation);

	if (m_outputLayer.contains(innovation))
		return m_outputLayer.neuron(innovation);

	return nullptr;
}
