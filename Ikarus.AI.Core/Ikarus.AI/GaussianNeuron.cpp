/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.20                                             */
/* Description : Neuron class with gaussian activation function         */
/*                                                                      */
/************************************************************************/
#include "stdafx.h"
#include "GaussianNeuron.h"
#include "NeuronFactory.h"
using namespace IK_AI_NAMESPACE;
IK_REGISTER_NEURON(GaussianNeuron);

void GaussianNeuron::run()
{
	setOutput(function(input()));
}

Constants::PrecisionType GaussianNeuron::function(const Constants::PrecisionType x)
{
	return std::exp((x * x) / -2.0) / std::sqrt(2 * std::_Pi);
}

Constants::PrecisionType GaussianNeuron::derivative(const Constants::PrecisionType x)
{
	const auto sh = 1.0 / std::cosh(x);
	return sh * sh;
}
