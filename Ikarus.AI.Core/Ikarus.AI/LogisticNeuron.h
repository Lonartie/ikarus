/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.20                                             */
/* Description : Neuron class with logistic ac. function (0 to 1)			*/
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"
#include "Neuron.h"

namespace IK_AI_NAMESPACE
{
	/// @brief Neuron with logistic sigmoid activation function
	/// @image html logistic.png width=75%
	class IK_AI_EXPORT LogisticNeuron final : public Neuron
	{
		NEURON(LogisticNeuron);
		
	public:
		using Neuron::Neuron;
		
		/// @copydoc Neuron::run
		void run() override;
		
		/// @copydoc Neuron::function
		Constants::PrecisionType function(const Constants::PrecisionType x) override;
		
		/// @copydoc Neuron::derivative
		Constants::PrecisionType derivative(const Constants::PrecisionType x) override;
	};
}
