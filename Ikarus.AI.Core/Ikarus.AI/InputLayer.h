/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.19                                             */
/* Description : Input layer for neurons that receive the input data    */
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"
#include "Layer.h"
#include "Constants.h"

namespace IK_AI_NAMESPACE
{
	/// @brief The input layer is the first layer of a neural network and stores the input neurons.
	/// The input neurons are the first neurons that receive the input data. They also dont process
	/// anything and just pass their given value directly to their connected neuron (through a genome).
	class IK_AI_EXPORT InputLayer final : public Layer
	{
		MEMORY(InputLayer);

	public:
		using Layer::Layer;
		using Layer::operator=;
		
		/// @brief Runs the given network
		/// @throws AccessViolation Eventually, if the input size doesn't match the neurons size
		/// @param input The input data this neural network
		void run(const std::vector<Constants::PrecisionType> & input);
	};
}