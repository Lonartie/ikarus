/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.20                                             */
/* Description : Neuron class with binary ac. function (0 or 1)			*/
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"
#include "Neuron.h"

namespace IK_AI_NAMESPACE
{
	/// @brief Neuron with binary activation function
	/// @image html binary.png width=75%
	class IK_AI_EXPORT BinaryNeuron final : public Neuron
	{
		NEURON(BinaryNeuron);
		
	public:
		using Neuron::Neuron;
		
		/// @copydoc Neuron::run
		void run() override;
		
		/// @copydoc Neuron::function
		Constants::PrecisionType function(const Constants::PrecisionType x) override;
		
		/// @copydoc Neuron::derivative
		Constants::PrecisionType derivative(const Constants::PrecisionType) override;

	};
}
