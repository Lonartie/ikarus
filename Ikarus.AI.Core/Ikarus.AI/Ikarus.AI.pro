TEMPLATE = lib                                                          
TARGET = Ikarus.AI                                                            
QT += core                                                                
CONFIG +=  c++1z                                                     
DEFINES += _UNICODE _ENABLE_EXTENDED_ALIGNED_STORAGE WIN64 IKARUSAI_LIB          
                                                                       
INCLUDEPATH += \                                                      
    ./GeneratedFiles \                                                
    . \                                                               
    ../ \                                                             
    ../../ \                                                          
                                                                       
HEADERS += \
    ./aiexport.h \
    ./stdafx.h \
    ./version.h \
    ./genome.h \
    ./inputlayer.h \
    ./layer.h \
    ./neuron.h \
    ./neuronfactory.h \
    ./neuronprobabilities.h \
     \
    ./arctanneuron.h \
    ./binaryneuron.h \
    ./logisticneuron.h \
    ./reluneuron.h \
    ./tanhneuron.h \
    ./biasneuron.h \
    ./hiddenlayer.h \
    ./outputlayer.h \
    ./simpleneuron.h \
    ./neuralnetwork.h \
    ./supervisedtrainer.h \
    ./trainer.h \
    ./trainingstructs.h \
    ./constants.h \
    ./gaussianneuron.h \
    ./bgdtrainer.h \
    ./sgdtrainer.h \
    ./gradientdescent.h \
    ./maths.h \
    ./dataencoding.h \
    ./onehot.h
                                                                       
SOURCES += \
    ./stdafx.cpp \
    ./version.cpp \
    ./genome.cpp \
    ./inputlayer.cpp \
    ./layer.cpp \
    ./neuron.cpp \
    ./neuronfactory.cpp \
     \
    ./arctanneuron.cpp \
    ./binaryneuron.cpp \
    ./logisticneuron.cpp \
    ./reluneuron.cpp \
    ./tanhneuron.cpp \
    ./biasneuron.cpp \
    ./simpleneuron.cpp \
    ./neuralnetwork.cpp \
    ./supervisedtrainer.cpp \
    ./trainer.cpp \
    ./gaussianneuron.cpp \
    ./bgdtrainer.cpp \
    ./sgdtrainer.cpp \
    ./gradientdescent.cpp \
    ./maths.cpp
                                                                       
FORMS +=
                                                                       
RESOURCES +=
                                                                       
# general build paths                                                  
UI_DIR += ./GeneratedFiles                                             
RCC_DIR += ./GeneratedFiles                                            
DEPENDPATH += .                                                        
OBJECTS_DIR += debug                                                   
CONFIG(debug, debug|release) {                                         
    DESTDIR = ../../../../x64/debug                                    
    INCLUDEPATH += ./GeneratedFiles/debug                              
    MOC_DIR += ./GeneratedFiles/debug                                  
} else {                                                               
    DESTDIR = ../../../../x64/release                                  
    INCLUDEPATH += ./GeneratedFiles/release                            
    MOC_DIR += ./GeneratedFiles/release                                
}                                                                      
                                                                       
#Ikarus.Common deps                                                               
INCLUDEPATH += ../../Ikarus.Common.Core/                                               
CONFIG(debug, debug|release) {                                         
    LIBS += -L../../../../x64/debug -lIkarus.Common                               
} else {                                                               
    LIBS += -L../../../../x64/release -lIkarus.Common                             
}                                                                      

#Ikarus.Communication deps                                                               
INCLUDEPATH += ../../Ikarus.Communication.Core/                                               
CONFIG(debug, debug|release) {                                         
    LIBS += -L../../../../x64/debug -lIkarus.Communication                               
} else {                                                               
    LIBS += -L../../../../x64/release -lIkarus.Communication                             
}                                                                      

#Ikarus.OpenCL deps                                                               
INCLUDEPATH += ../../Ikarus.OpenCL.Core/                                               
CONFIG(debug, debug|release) {                                         
    LIBS += -L../../../../x64/debug -lIkarus.OpenCL                               
} else {                                                               
    LIBS += -L../../../../x64/release -lIkarus.OpenCL                             
}                                                                      

                                                                     
