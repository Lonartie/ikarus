/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.20                                             */
/* Description : Neuron class with gaussian activation function         */
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"
#include "Neuron.h"

namespace IK_AI_NAMESPACE
{
	/// @brief Neuron with tanh activation function
	/// @image html gaussian.png width=75%
	class IK_AI_EXPORT GaussianNeuron final : public Neuron
	{
		NEURON(GaussianNeuron);
		
	public:
		using Neuron::Neuron;
		
		/// @copydoc Neuron::run
		void run() override;
		
		/// @copydoc Neuron::function
		Constants::PrecisionType function(const Constants::PrecisionType x) override;
		
		/// @copydoc Neuron::derivative
		Constants::PrecisionType derivative(const Constants::PrecisionType x) override;
	};
}
