/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.19                                             */
/* Description : Creates a neuron based on the given probabilities      */
/*                                                                      */
/************************************************************************/
#include "stdafx.h"
#include "NeuronFactory.h"
#include "Ikarus.Common/Random.h"
using namespace IK_AI_NAMESPACE;
using namespace IK_NAMESPACE(Common);

unsigned NeuronFactory::RegisteredNeurons()
{
	return creatorFunctions().size();
}

const QStringList& NeuronFactory::RegisteredNeuronNames()
{
	return creatorNames();
}

Neuron::UPtr NeuronFactory::Create(const NeuronProbabilities& probabilities)
{
	uint64_t sum = 0;
	uint64_t index = 0;
	std::type_index indexFound = typeid(void);

	// sum up all probabilities
	for(const auto& p : probabilities)
	{
		sum += p.second.value();
	}

	// create random value between 0 and sum of all probabilities
	auto rand = Random<int64_t>(0, static_cast<uint64_t>(sum));

	// check where this random value fell on
	for(const auto& p : probabilities)
	{
		rand -= p.second.value();
		if (rand <= 0)
		{
			indexFound = p.first;
			break;
		}
	}

	// find the correct creator
	for (uint64_t n = 0; n < creatorIndices().size(); n++)
	{
		if (creatorIndices()[n].hash_code() == indexFound.hash_code())
		{
			index = n;
			break;
		}
	}

	// create and return the neuron
	return std::invoke(creatorFunctions()[index]);
}

Neuron::UPtr NeuronFactory::Create(const QString& name)
{
	const auto index = creatorNames().indexOf(name.toLower());
	return std::invoke(creatorFunctions()[index]);
}

const std::vector<NeuronFactory::NeuronCreator>& NeuronFactory::neuronCreators()
{
	return creatorFunctions();
}

std::vector<NeuronFactory::NeuronCreator>& NeuronFactory::creatorFunctions()
{
	static std::vector<NeuronCreator> __creators;
	return __creators;
}

std::vector<std::type_index>& NeuronFactory::creatorIndices()
{
	static std::vector<std::type_index> __indices;
	return __indices;
}

QStringList& NeuronFactory::creatorNames()
{
	static QStringList __names;
	return __names;
}
