#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(IKARUSAI_LIB)
#  define IK_AI_EXPORT Q_DECL_EXPORT
# else
#  define IK_AI_EXPORT Q_DECL_IMPORT
# endif
#else
# define IK_AI_EXPORT
#endif

#define IK_AI_NAMESPACE Ikarus::AI