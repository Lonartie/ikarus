/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.19                                             */
/* Description : Genome class representing human genomes (connections)  */
/*                                                                      */
/************************************************************************/
#include "stdafx.h"
#include "Genome.h"
#include "Neuron.h"
using namespace IK_AI_NAMESPACE;

Constants::PrecisionType Genome::output() const
{
	return m_output;
}

Constants::PrecisionType Genome::input() const
{
	return m_input;
}

void Genome::setInput(const Constants::PrecisionType input)
{
	m_input = input;
}

const std::shared_ptr<Neuron>& Genome::inputNeuron() const
{
	return m_inputNeuron;
}

void Genome::setInputNeuron(const std::shared_ptr<Neuron>& inputNeuron)
{
	m_inputNeuron = inputNeuron;
	m_inputNeuronInnovation = m_inputNeuron->innovation();
}

const std::shared_ptr<Neuron>& Genome::outputNeuron() const
{
	return m_outputNeuron;
}

void Genome::setOutputNeuron(const std::shared_ptr<Neuron>& outputNeuron)
{
	m_outputNeuron = outputNeuron;
	m_outputNeuronInnovation = m_outputNeuron->innovation();
}

uint64_t Genome::UniqueID()
{
	static uint64_t __ID = 0;
	return __ID++;
}

Genome::Genome()
{
	m_ID = UniqueID();
}

uint64_t Genome::ID() const
{
	return m_ID;
}

bool Genome::active() const
{
	return m_active;
}

void Genome::setActive(const bool active)
{
	m_active = active;
}

Constants::PrecisionType Genome::weight() const
{
	return m_weight;
}

void Genome::setWeight(const Constants::PrecisionType weight)
{
	m_weight = weight;
}

void Genome::adjustWeight(const Constants::PrecisionType adjustment)
{
	m_weight += adjustment;
}

void Genome::run()
{
	if(!m_active)
		return;
	
	m_output = m_input * m_weight;
	m_outputNeuron->addInput(m_output);
}

QJsonObject Genome::serialize() const
{
	QJsonObject output;

	output.insert("ID", QString::number(m_ID));
	output.insert("Active", m_active);
	output.insert("Weight", m_weight);
	output.insert("LastInput", m_input);
	output.insert("LastOutput", m_output);
	output.insert("InputNeuronInnovation", QString::number(m_inputNeuronInnovation));
	output.insert("OutputNeuronInnovation", QString::number(m_outputNeuronInnovation));
	
	return output;
}

bool Genome::deserialize(const QJsonObject& object)
{
	m_ID = object["ID"].toString().toULongLong();
	m_active = object["Active"].toBool();
	m_weight = object["Weight"].toVariant().value<Constants::PrecisionType>();
	m_input = object["LastInput"].toVariant().value<Constants::PrecisionType>();
	m_output = object["LastOutput"].toVariant().value<Constants::PrecisionType>();
	m_inputNeuronInnovation = object["InputNeuronInnovation"].toString().toULongLong();
	m_outputNeuronInnovation = object["OutputNeuronInnovation"].toString().toULongLong();	
	
	return true;
}

uint64_t Genome::inputNeuronInnovation() const
{
	return m_inputNeuronInnovation;
}

uint64_t Genome::outputNeuronInnovation() const
{
	return m_outputNeuronInnovation;
}
