#include "stdafx.h"
#include "Version.h"
#include "Ikarus.Common/VersioningSystem.h"

namespace IK_AI_NAMESPACE::Version
{
	IK_EXPORT(AI) const char* BuildDate = "2006261730";
	IK_EXPORT(AI) const char* LastCommitID = "162f21e03acceb56337f76c636afe0852bb6fb25";
	IK_EXPORT(AI) const char* BuildBranch = "master";
	IK_EXPORT(AI) const char* AssemblyName = "Ikarus.AI";
	
	REGISTER_ASSEMBLY(BuildDate, LastCommitID, BuildBranch, AssemblyName);
}
