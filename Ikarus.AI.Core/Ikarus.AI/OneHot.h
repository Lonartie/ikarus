/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.26                                             */
/* Description : OneHot encoding algorithm                              */
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"
#include "DataEncoding.h"

namespace IK_AI_NAMESPACE
{
	/// @copydoc Ikarus::AI::DataEncoding<T>
	/// @brief
	/// @note Requires output between -1 and 1
	/// @tparam T 
	template<typename T>
	class OneHot: public DataEncoding<T>
	{
	public:

		/// @copydoc Ikarus::AI::DataEncoding<T>::analyze() const
		uint64_t analyze() const override;

		/// @copydoc Ikarus::AI::DataEncoding<T>::decode(const std::vector<Constants::PrecisionType>&)
		const T& decode(const std::vector<Constants::PrecisionType>& out) override;

		/// @copydoc Ikarus::AI::DataEncoding<T>::encode(const T& in) const
		std::vector<Constants::PrecisionType> encode(const T& in) const override;

	private:

		uint64_t toIndex(const T& o) const;
		const T& fromIndex(uint64_t i) const;

		uint64_t m_flagsPerCell = 8;

	};

	template <typename T>
	uint64_t OneHot<T>::analyze() const
	{
		return std::ceil(std::log(data().size()) / std::log(m_flagsPerCell));
	}

	template <typename T>
	const T& OneHot<T>::decode(const std::vector<Constants::PrecisionType>& out)
	{
		uint64_t index = 0;
		uint64_t n = 0;
		for (const auto& o : out)
		{
			const auto clamped = std::clamp<Constants::PrecisionType>(o, -1, 1);
			const auto p = (clamped + 1) / 2;
			const auto pi = p * (m_flagsPerCell - 1);
			const auto di = std::round(pi * std::pow(m_flagsPerCell, n++));
			index += di;
		}
		return fromIndex(index);
	}

	template <typename T>
	std::vector<Constants::PrecisionType> OneHot<T>::encode(const T& in) const
	{
		auto index = toIndex(in);
		auto itmp = index;
		std::vector<Constants::PrecisionType> out;
		out.resize(analyze(), 0);

		for (uint64_t n = 0; n < index && itmp > 0; ++n, --itmp)
		{
			for (uint64_t i = 0; i < out.size(); i++)
				if (out[i] >= 8)
				{
					out[i] = 0;
					out[i + 1]++;
				}
				else
					break;
			
			out[0]++;
		}

		for (auto& o : out)
			o = o / (m_flagsPerCell - 1.0) * 2 - 1;
		
		return out;
	}

	template <typename T>
	uint64_t OneHot<T>::toIndex(const T& o) const
	{
		auto& list = data();
		for (uint64_t i = 0; i < list.size(); i++)
			if (list[i] == o)
				return i;
		return -1;
	}

	template <typename T>
	const T& OneHot<T>::fromIndex(uint64_t i) const
	{
		return data()[i];
	}
}
