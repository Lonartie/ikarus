/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.19                                             */
/* Description : Provied probabilities for registered neuron classes    */
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"
#include "Ikarus.Common/Ranged.h"

namespace IK_AI_NAMESPACE
{
	/// @brief NeuronProbabilities is a map of a type and a ranged value between 0 and 100 [percentage]
	using NeuronProbabilities = std::map<std::type_index, IK_NAMESPACE(Common)::Ranged<0, 100>>;
}

