/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.21                                             */
/* Description : Supervised trainer for neural networks                 */
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"
#include "Trainer.h"
#include "TrainingStructs.h"

namespace IK_AI_NAMESPACE
{
	class IK_AI_EXPORT SupervisedTrainer: public Trainer
	{
	public:
		using Trainer::Trainer;
		using Trainer::operator=;

		/// @brief Trains the neural network towards reaching the desired outcomes from the table while respecting the settings
		/// @param table The input - target map of your desired outcome
		/// @param settings The desired error and other behavioral settings
		virtual SGDResult train(const TrainingTable& table, const SGDSettings& settings) = 0;

		/// @brief Calculates the accuracy in percent this neural network archived
		/// @param table The input and output set used for calculation
		/// @return The accuracy in percent
		float accuracy(const TrainingTable& table);

	};
}