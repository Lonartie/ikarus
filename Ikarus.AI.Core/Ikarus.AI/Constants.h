/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.19                                             */
/* Description : General constants used by neural networks              */
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"

namespace IK_AI_NAMESPACE
{
	inline struct Constants
	{
		using PrecisionType = float;

		
	} Constants;
}