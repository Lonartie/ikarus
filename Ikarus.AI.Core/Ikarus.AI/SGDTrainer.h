/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.22                                             */
/* Description : Stochastic gradient descent supervised nn trainer      */
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"
#include "SupervisedTrainer.h"
#include <unordered_map>

namespace IK_AI_NAMESPACE
{
	/// @brief Stochastic gradient descent neural network trainer
	class IK_AI_EXPORT SGDTrainer: public SupervisedTrainer
	{
	public:
		using ErrorMap = std::vector<Constants::PrecisionType>;

		using SupervisedTrainer::SupervisedTrainer;
		using SupervisedTrainer::operator=;

		SGDResult train(const TrainingTable& table, const SGDSettings& settings) override;

	private:
		SGDResult train(const TrainingSet& set, Constants::PrecisionType learningRate);

		std::vector<Constants::PrecisionType> m_errorMap;

	};
}