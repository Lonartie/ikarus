#include <boost/test/unit_test.hpp>
#include "Ikarus.AI/stdafx.h"
#include "Ikarus.AI/OneHot.h"
using namespace IK_AI_NAMESPACE;

#define BOOST_CHECK_EQUAL_LISTS(L, R) \
{ \
	std::vector __l = L; \
	std::vector __r = R; \
	BOOST_CHECK_EQUAL_COLLECTIONS(__l.begin(), __l.end(), __r.begin(), __r.end()); \
}

namespace
{
	std::vector<QString> SAMPLE_SET // size: 15
	{ 
		"Leon","Lukas","Kilian","Markus","Felix","Justin","Martin","Erik","Amadeus","Salieri","Ikarus","Paul","Daniel","John","Richard"
	};
}

BOOST_AUTO_TEST_SUITE(TSC_DataEncoding);
BOOST_AUTO_TEST_SUITE(TS_OneHot);

BOOST_AUTO_TEST_CASE(RequiredCellSize)
{
	OneHot<QString> onehot;
	onehot.setData(SAMPLE_SET);
	BOOST_CHECK_EQUAL(2, onehot.analyze());
}

BOOST_AUTO_TEST_CASE(Encoding)
{
	OneHot<QString> onehot;
	onehot.setData(SAMPLE_SET);
	onehot.analyze();

	BOOST_CHECK_EQUAL_LISTS(onehot.encode("Leon"), (std::vector {-1.00f, -1.00f}));
	BOOST_CHECK_EQUAL_LISTS(onehot.encode("Richard"), (std::vector {6 / 7.f * 2 - 1, 1 / 7.f * 2 - 1}));
}

BOOST_AUTO_TEST_CASE(Decoding)
{
	OneHot<QString> onehot;
	onehot.setData(SAMPLE_SET);
	onehot.analyze();
	
	BOOST_CHECK_EQUAL(onehot.decode(std::vector{-1.00f, -1.00f}).toStdString(), "Leon");
	BOOST_CHECK_EQUAL(onehot.decode(std::vector{6 / 7.f * 2 - 1, 1 / 7.f * 2 - 1}).toStdString(), "Richard");
}

BOOST_AUTO_TEST_SUITE_END();
BOOST_AUTO_TEST_SUITE_END();
