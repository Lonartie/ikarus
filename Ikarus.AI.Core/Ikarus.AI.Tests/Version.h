/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.05.24                                             */
/* Description : Versioning file                                        */
/*                                                                      */
/************************************************************************/
#pragma once
#include "Ikarus.AI/stdafx.h"

namespace IK_AI_NAMESPACE::Tests::Version
{
	extern const char* BuildDate;
	extern const char* LastCommitID;
	extern const char* BuildBranch;
	extern const char* AssemblyName;
}                                                                                                    