#pragma once
#include "Ikarus.AI/NeuralNetwork.h"
#include "Ikarus.AI/SupervisedTrainer.h"

#include "Ikarus.AI.Widgets/qcustomplot.h"

namespace IK_AI_NAMESPACE::Tests
{	
	class TestPlotFixture
	{
	public:
		TestPlotFixture();
		~TestPlotFixture();

		void init();

		void setName(const QString& name);
		void setNetwork(NeuralNetwork&& network);
		void setTrainer(SupervisedTrainer& trainer);		

		void train(TrainingTable table);
		NeuralNetwork& network();
		
	private:

		static QString initializeQApplication();
		
		void addDataPoint(SGDResult result);

		void printConsoleResult();
		void printPlotResult();
		void saveNetwork();

		QCustomPlot* m_plot = nullptr;
		SGDSettings m_settings;
		SGDResult m_result;
		QString m_name;
		NeuralNetwork m_network;
		TrainingTable m_table;
		SupervisedTrainer* m_trainer = nullptr;
		QString m_execFolder;
		double m_time = 0;
		
	};
}