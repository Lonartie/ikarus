#include <boost/test/unit_test.hpp>
#include "Ikarus.AI/NeuralNetwork.h"
#include "Ikarus.AI/NeuronFactory.h"
using namespace IK_AI_NAMESPACE;

BOOST_AUTO_TEST_SUITE(TS_NeuralNetworkSerialization);

BOOST_AUTO_TEST_CASE(SerializeDeserializeSameResult)
{
	NeuralNetwork network;
	network.addHiddenLayer(1);

	auto neuron1 = network.addNeuron(NeuronFactory::Create("Simple"), 0);
	auto neuron2 = network.addNeuron(NeuronFactory::Create("TanH"), 1);
	auto neuron3 = network.addNeuron(NeuronFactory::Create("Logistic"), 2);

	network.fullyConnect();
	network.inputLayer().genomes()[0]->setWeight(1.5);
	network.hiddenLayers()[0].genomes()[0]->setWeight(2);

	BOOST_CHECK_CLOSE_FRACTION(1.0 / (1.0 + std::exp(-2 * std::tanh(1.5))), network.run({1})[0], 1e-5);
	BOOST_CHECK_CLOSE_FRACTION(1.0 / (1.0 + std::exp(-2 * std::tanh(1.5))), network.run({1})[0], 1e-5);
	BOOST_CHECK_CLOSE_FRACTION(1.0 / (1.0 + std::exp(-2 * std::tanh(1.5))), network.run({1})[0], 1e-5);

	const auto serialized = network.save();


	NeuralNetwork copy;

	copy.load(serialized);

	BOOST_CHECK_CLOSE_FRACTION(1.0 / (1.0 + std::exp(-2 * std::tanh(1.5))), copy.run({1})[0], 1e-5);
	BOOST_CHECK_CLOSE_FRACTION(1.0 / (1.0 + std::exp(-2 * std::tanh(1.5))), copy.run({1})[0], 1e-5);
	BOOST_CHECK_CLOSE_FRACTION(1.0 / (1.0 + std::exp(-2 * std::tanh(1.5))), copy.run({1})[0], 1e-5);

	const auto serialized2 = copy.save();

	BOOST_CHECK(serialized == serialized2);
}

BOOST_AUTO_TEST_SUITE_END();