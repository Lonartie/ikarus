/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.06.20                                             */
/* Description : Tests for the NeuronFactory                            */
/*                                                                      */
/************************************************************************/
#include <boost/test/unit_test.hpp>
#include "Ikarus.AI/NeuronFactory.h"
using namespace IK_AI_NAMESPACE;

BOOST_AUTO_TEST_SUITE(TS_NeuronFactory);

BOOST_AUTO_TEST_CASE(ContainsBasicNeuronTypes)
{
	const auto& names = NeuronFactory::RegisteredNeuronNames();

	BOOST_CHECK(names.contains("TanH", Qt::CaseInsensitive));
	BOOST_CHECK(names.contains("ArcTan", Qt::CaseInsensitive));
	BOOST_CHECK(names.contains("ReLU", Qt::CaseInsensitive));
	BOOST_CHECK(names.contains("Binary", Qt::CaseInsensitive));
	BOOST_CHECK(names.contains("Logistic", Qt::CaseInsensitive));
	BOOST_CHECK(names.contains("Bias", Qt::CaseInsensitive));
	BOOST_CHECK(names.contains("Simple", Qt::CaseInsensitive));
}

BOOST_AUTO_TEST_SUITE_END();
