TEMPLATE = app                                                          
TARGET = Ikarus.AI.Tests                                                            
QT += core                                                                
CONFIG += console c++1z                                                     
DEFINES += _UNICODE _ENABLE_EXTENDED_ALIGNED_STORAGE WIN64           
                                                                       
INCLUDEPATH += \                                                      
    ./GeneratedFiles \                                                
    . \                                                               
    ../ \                                                             
    ../../ \                                                          
                                                                       
HEADERS += \
    ./version.h \
    ./testplotfixture.h
                                                                       
SOURCES += \
    ./main.cpp \
    ./version.cpp \
    ./neuronfactorytests.cpp \
    ./basictests.cpp \
    ./serializationtests.cpp \
    ./simplenetworkstests.cpp \
    ./onehottests.cpp \
    ./testplotfixture.cpp
                                                                       
FORMS +=
                                                                       
RESOURCES +=
                                                                       
# general build paths                                                  
UI_DIR += ./GeneratedFiles                                             
RCC_DIR += ./GeneratedFiles                                            
DEPENDPATH += .                                                        
OBJECTS_DIR += debug                                                   
CONFIG(debug, debug|release) {                                         
    DESTDIR = ../../../../x64/debug                                    
    INCLUDEPATH += ./GeneratedFiles/debug                              
    MOC_DIR += ./GeneratedFiles/debug                                  
} else {                                                               
    DESTDIR = ../../../../x64/release                                  
    INCLUDEPATH += ./GeneratedFiles/release                            
    MOC_DIR += ./GeneratedFiles/release                                
}                                                                      
                                                                       
#boost deps                                                        
win32 {                                                            
    INCLUDEPATH += $(BOOST_INC_DIR)                                
    LIBS += -L$(BOOST_LIB_DIR)                                     
} macx {                                                           
    INCLUDEPATH += /usr/local/include                              
    DEFINES += BOOST_TEST_DYN_LINK                                 
    LIBS += -L/usr/local/lib -lboost_unit_test_framework           
} unix:!macx {                                                     
}                                                                  

#Ikarus.AI deps                                                               
INCLUDEPATH += ../../Ikarus.AI.Core/                                               
CONFIG(debug, debug|release) {                                         
    LIBS += -L../../../../x64/debug -lIkarus.AI                               
} else {                                                               
    LIBS += -L../../../../x64/release -lIkarus.AI                             
}                                                                      

#Ikarus.OpenCL deps                                                               
INCLUDEPATH += ../../Ikarus.OpenCL.Core/                                               
CONFIG(debug, debug|release) {                                         
    LIBS += -L../../../../x64/debug -lIkarus.OpenCL                               
} else {                                                               
    LIBS += -L../../../../x64/release -lIkarus.OpenCL                             
}                                                                      

#Ikarus.Communication deps                                                               
INCLUDEPATH += ../../Ikarus.Communication.Core/                                               
CONFIG(debug, debug|release) {                                         
    LIBS += -L../../../../x64/debug -lIkarus.Communication                               
} else {                                                               
    LIBS += -L../../../../x64/release -lIkarus.Communication                             
}                                                                      

#Ikarus.Common deps                                                               
INCLUDEPATH += ../../Ikarus.Common.Core/                                               
CONFIG(debug, debug|release) {                                         
    LIBS += -L../../../../x64/debug -lIkarus.Common                               
} else {                                                               
    LIBS += -L../../../../x64/release -lIkarus.Common                             
}                                                                      

                                                                     
