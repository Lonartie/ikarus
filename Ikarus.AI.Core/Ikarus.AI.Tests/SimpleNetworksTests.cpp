#include <boost/test/unit_test.hpp>
#include "TestPlotFixture.h"
#include "Ikarus.AI/stdafx.h"
#include "Ikarus.AI/NeuralNetwork.h"
#include "Ikarus.AI/SGDTrainer.h"
#include "Ikarus.Common/Random.h"
#include "Ikarus.Common/Vector.h"
using namespace IK_AI_NAMESPACE;
using namespace IK_AI_NAMESPACE::Tests;
using namespace IK_Common_NAMESPACE;

namespace
{
	static auto random_helper = [] { srand(time(nullptr)); return true; }();
}

BOOST_FIXTURE_TEST_SUITE(TS_SimpleNetworks, TestPlotFixture);

BOOST_AUTO_TEST_CASE(LinearRegression)
{
	init();
	using pt = Constants::PrecisionType;
	auto testFn = [](double x) { return 0.5 * x - 0.1; };

	setNetwork(NeuralNetwork::Create(
		std::tuple{2, "simple"},
		std::tuple{4, "tanh"},
		std::tuple{1, "logistic"}
	));
	
	// create test data
	SGDTrainer trainer(network());
	TrainingTable table;
	for (int i = 0; i < 1e2; ++i)
	{
		const auto rx = Random(-1.0, 1.0);
		const auto ry = Random(-1.0, 1.0);
		const auto is = testFn(rx) > ry;
		TrainingSet set;
		set.Inputs = {pt(rx), pt(ry)};
		set.Targets = {pt(is ? 1 : 0)};
		table.Sets.push_back(set);
	}

	setTrainer(trainer);
	setName("LinearRegression");
	train(table);
}

BOOST_AUTO_TEST_CASE(XOR)
{
	init();
	using pt = Constants::PrecisionType;

	setNetwork(NeuralNetwork::Create(
		layer(2, "simple"),
		layer(3, "tanh"),
		layer(1, "logistic")
	));

	static const TrainingTable table
	{
		{
			{ { 0, 0 }, { 0 } },
			{ { 1, 0 }, { 1 } },
			{ { 0, 1 }, { 1 } },
			{ { 1, 1 }, { 0 } }
		}
	};

	SGDTrainer trainer(network());
	setName("XOR");
	setTrainer(trainer);
	train(table);
}

BOOST_AUTO_TEST_CASE(ColorIntensityPredictor)
{
	init();
	using pt = Constants::PrecisionType;
	setNetwork(NeuralNetwork::Create(
		layer(3, "simple"),
		layer(4, "tanh"),
		layer(1, "logistic")
	));

	SGDTrainer trainer(network());

	TrainingTable table;
	for (int i = 0; i < 1e2; i++)
	{
		TrainingSet set;
		set.Inputs = {Random<pt>(0, 1), Random<pt>(0, 1), Random<pt>(0, 1)};
		set.Targets = {pt(SumVector(set.Inputs) > 1.5 ? 1 : 0)};
		table.Sets.push_back(std::move(set));
	}

	setTrainer(trainer);
	setName("ColorIntensityPredictor");
	train(table);
}

BOOST_AUTO_TEST_SUITE_END();