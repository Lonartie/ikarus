#include "Version.h"
#include "Ikarus.Common/VersioningSystem.h"

namespace IK_AI_NAMESPACE::Tests::Version
{
	const char* BuildDate = "2006271558";
	const char* LastCommitID = "a098a08e3dde73b5dd80517591b2978d85735f1c";
	const char* BuildBranch = "master";
	const char* AssemblyName = "Ikarus.AI.Tests";
	
	REGISTER_ASSEMBLY(BuildDate, LastCommitID, BuildBranch, AssemblyName);
}
