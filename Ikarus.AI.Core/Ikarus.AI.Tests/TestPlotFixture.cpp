#include "TestPlotFixture.h"
#include <boost/test/unit_test.hpp>
using namespace IK_AI_NAMESPACE;
using namespace IK_AI_NAMESPACE::Tests;

QString TestPlotFixture::initializeQApplication()
{
	static auto& ms = boost::unit_test::framework::master_test_suite();
	static QApplication __app(ms.argc, ms.argv);

	return QFileInfo(ms.argv[0]).absoluteDir().absolutePath();
}

TestPlotFixture::TestPlotFixture()
{
	m_execFolder = initializeQApplication();
	m_plot = new QCustomPlot();	
}

TestPlotFixture::~TestPlotFixture()
{
	delete m_plot;
}

void TestPlotFixture::init()
{
	m_settings = SGDSettings{
		uint64_t(1e5),
		uint64_t(50),
		true,
		Constants::PrecisionType(.01),
		Constants::PrecisionType(.1),
		[this](auto tr) { addDataPoint(tr); }
	};

	QDir().mkpath(m_execFolder + "/TestResults/TrainedModels/");
	QDir().mkpath(m_execFolder + "/TestResults/TrainingData/");

	m_plot->clearGraphs();
	m_plot->addGraph();
	m_plot->addGraph(m_plot->xAxis, m_plot->yAxis2);
	m_plot->setFixedSize(QSize(1600, 900));
	m_plot->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDisc, QPen(QColor(255, 0, 0)), QBrush(Qt::SolidPattern), 5));
	m_plot->graph(0)->setPen(QPen(QBrush(QColor(255, 0, 0)), 1));
	m_plot->graph(1)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssDiamond, QPen(QColor(0, 0, 255)), QBrush(Qt::SolidPattern), 5));
	m_plot->graph(1)->setPen(QPen(QBrush(QColor(0, 0, 255)), 1));
	m_plot->xAxis->setLabel("Iterations");
	m_plot->yAxis->setLabel("Cost");
	m_plot->yAxis2->setLabel("Accuracy");
	m_plot->legend->setVisible(true);
	m_plot->yAxis2->setVisible(true);
	m_plot->axisRect()->insetLayout()->setInsetAlignment(0, Qt::AlignLeft | Qt::AlignTop);
}

void TestPlotFixture::setName(const QString& name)
{
	m_name = name;
	m_plot->graph(0)->setName(m_name + " - Cost");
	m_plot->graph(1)->setName(m_name + " - Accuracy");
}

void TestPlotFixture::setNetwork(NeuralNetwork&& network)
{
	m_network = std::move(network);
	m_network.fullyConnect();
}

void TestPlotFixture::setTrainer(SupervisedTrainer& trainer)
{
	m_trainer = &trainer;
}

void TestPlotFixture::addDataPoint(const SGDResult result)
{
	m_plot->graph(0)->addData(result.IterationsRan, result.Cost);
	m_plot->graph(1)->addData(result.IterationsRan, m_trainer->accuracy(m_table));
}

void TestPlotFixture::train(TrainingTable table)
{
	m_table = std::move(table);

	const auto begin = std::chrono::system_clock::now();
	m_result = m_trainer->train(m_table, m_settings);
	const auto end = std::chrono::system_clock::now();

	m_time = std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() / 1000.0f;

	
	printConsoleResult();
	printPlotResult();
	saveNetwork();
}

NeuralNetwork& TestPlotFixture::network()
{
	return m_network;
}

void TestPlotFixture::printConsoleResult()
{
	BOOST_CHECK_EQUAL(true, m_result.CostGoalReached);
	BOOST_TEST_MESSAGE("shuffle inputs: " << (m_settings.ShuffleInputSets ? "yes" : "no"));
	BOOST_TEST_MESSAGE("learning rate: " << m_settings.LearningRate);
	BOOST_TEST_MESSAGE("sample size: " << m_table.Sets.size());
	BOOST_TEST_MESSAGE("epochs: " << m_settings.Epochs);
	BOOST_TEST_MESSAGE("-------------- RESULT --------------");
	BOOST_TEST_MESSAGE("accuracy: " << m_trainer->accuracy(m_table) << "%");
	BOOST_TEST_MESSAGE("total cost: " << m_result.Cost << " / " << m_settings.DesiredCost);
	BOOST_TEST_MESSAGE("total error: " << m_result.Error);
	BOOST_TEST_MESSAGE("iterations ran: " << m_result.IterationsRan << " / " << m_settings.MaxIterations);
	BOOST_TEST_MESSAGE("training time: " << m_time << " ms");
}

void TestPlotFixture::printPlotResult()
{
	m_plot->graph(0)->setName(m_plot->graph(0)->name() + " => " + QString::number(m_result.Cost));
	m_plot->graph(1)->setName(m_plot->graph(1)->name() + " => " + QString::number(m_trainer->accuracy(m_table)));
	
	m_plot->replot();
	m_plot->xAxis->rescale();
	m_plot->yAxis->rescale();
	m_plot->yAxis2->rescale();

	m_plot
		->grab()
		.toImage()
		.save(m_execFolder + "/TestResults/TrainingData/" + m_name + ".png", "PNG");
}

void TestPlotFixture::saveNetwork()
{
	m_network.saveTo(m_execFolder + "/TestResults/TrainedModels/" + m_name + ".json");
}
