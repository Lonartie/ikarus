#include <boost/test/unit_test.hpp>

#include "Ikarus.AI/NeuralNetwork.h"
#include "Ikarus.AI/NeuronFactory.h"
#include "Ikarus.AI/Genome.h"

#include "Ikarus.Common/Linq.h"
#include "Ikarus.Common/Random.h"

using namespace IK_AI_NAMESPACE;

BOOST_AUTO_TEST_SUITE(TS_BasicFunctions);

// input - calculation - output => ICO
BOOST_AUTO_TEST_CASE(IcoPrinciple)
{
	InputLayer inputLayer;
	HiddenLayer hiddenLayer;
	OutputLayer outputLayer;

	const auto neuron1 = inputLayer.addNeuron(NeuronFactory::Create("Simple"));
	const auto neuron2 = hiddenLayer.addNeuron(NeuronFactory::Create("TanH"));
	const auto neuron3 = outputLayer.addNeuron(NeuronFactory::Create("Logistic"));

	const auto genome1 = Neuron::Connect(neuron1, neuron2, 1.5);
	const auto genome2 = Neuron::Connect(neuron2, neuron3, 2);

	inputLayer.run({1});
		
	BOOST_CHECK_CLOSE_FRACTION(1, neuron1->output(), 1e-5);
	BOOST_CHECK_CLOSE_FRACTION(1, genome1->input(), 1e-5);
	BOOST_CHECK_CLOSE_FRACTION(1.5, genome1->output(), 1e-5);
	
	BOOST_REQUIRE(neuron2->input() != 0);

	hiddenLayer.run();

	BOOST_CHECK_CLOSE_FRACTION(std::tanh(1.5), neuron2->output(), 1e-5);
	BOOST_CHECK_CLOSE_FRACTION(std::tanh(1.5), genome2->input(), 1e-5);
	BOOST_CHECK_CLOSE_FRACTION(2 * std::tanh(1.5), genome2->output(), 1e-5);
		
	BOOST_REQUIRE(neuron3->input() != 0);

	outputLayer.run();

	BOOST_CHECK_CLOSE_FRACTION(1.0 / (1.0 + std::exp(-2 * std::tanh(1.5))), neuron3->output(), 1e-5);
}

BOOST_AUTO_TEST_CASE(SameWithNeuralNetwork)
{
	NeuralNetwork network;
	network.addHiddenLayer(1);

	auto neuron1 = network.addNeuron(NeuronFactory::Create("Simple"), 0);
	auto neuron2 = network.addNeuron(NeuronFactory::Create("TanH"), 1);
	auto neuron3 = network.addNeuron(NeuronFactory::Create("Logistic"), 2);

	network.fullyConnect();
	network.inputLayer().genomes()[0]->setWeight(1.5);
	network.hiddenLayers()[0].genomes()[0]->setWeight(2);

	BOOST_CHECK_CLOSE_FRACTION(1.0 / (1.0 + std::exp(-2 * std::tanh(1.5))), network.run({1})[0], 1e-5);
	BOOST_CHECK_CLOSE_FRACTION(1.0 / (1.0 + std::exp(-2 * std::tanh(1.5))), network.run({1})[0], 1e-5);
	BOOST_CHECK_CLOSE_FRACTION(1.0 / (1.0 + std::exp(-2 * std::tanh(1.5))), network.run({1})[0], 1e-5);
}

BOOST_AUTO_TEST_CASE(NeuralNetworkBehaviour)
{
	auto perceptron = NeuralNetwork::Create(1, 1, 1);
	BOOST_CHECK_EQUAL(3, perceptron.layers());
	BOOST_CHECK_EQUAL(1, perceptron.run({1}).size());
	BOOST_CHECK_EQUAL(perceptron.run({1.0f})[0], perceptron.run({2.0f})[0]);

	perceptron.fullyConnect();
	perceptron.inputLayer().genomes()[0]->setWeight(1);
	perceptron.hiddenLayers()[0].genomes()[0]->setWeight(2);

	BOOST_CHECK_NE(perceptron.run({1.0f})[0], perceptron.run({2.0f})[0]);
}

BOOST_AUTO_TEST_SUITE_END();