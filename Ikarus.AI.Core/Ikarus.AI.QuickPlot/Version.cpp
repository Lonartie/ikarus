#include "stdafx.h"
#include "Version.h"
#include "Ikarus.Common/VersioningSystem.h"

namespace Ikarus::AI::QuickPlot::Version
{
	const char* BuildDate = "2006261924";
	const char* LastCommitID = "162f21e03acceb56337f76c636afe0852bb6fb25";
	const char* BuildBranch = "master";
	const char* AssemblyName = "Ikarus.AI.QuickPlot";
	
	REGISTER_ASSEMBLY(BuildDate, LastCommitID, BuildBranch, AssemblyName);
}
