#pragma once

#include "Ikarus.AI.Widgets/PlotWidget.h"
#include <QObject>

class QPMenu : public QObject
{
	Q_OBJECT;

public:
	QPMenu(int argc, char** argv);

	bool run();

private:

	bool showHelp() const;

	bool error(const QString& msg) const;

	bool hasFlags() const;	
	bool hasFlag(const QString& flg) const;
	QString getFlag(const QString& flg) const;

	std::map<double, double> getMap(const QString& path, bool& ok);
	bool saveOutput(const QString& path);
	
private:
	QStringList m_args;
	IK_AI_WIDGETS_NAMESPACE::PlotWidget m_plot;
};
