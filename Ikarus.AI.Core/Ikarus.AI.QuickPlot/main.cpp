#include "stdafx.h"
#include "QPMenu.h"

#include <QtWidgets/QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);

	QPMenu menu(argc, argv);
	menu.run();
}
