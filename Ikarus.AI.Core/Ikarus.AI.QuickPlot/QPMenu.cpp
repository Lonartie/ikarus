#include "stdafx.h"
#include "QPMenu.h"

#include <iostream>
#include <QApplication>

using namespace IK_AI_WIDGETS_NAMESPACE;

QPMenu::QPMenu(int argc, char** argv)
{
	for (int i = 1; i < argc; i++)
	{
		auto arg = QString(argv[i]);

		while (arg.startsWith("-") || arg.startsWith("/"))
			arg.remove(0, 1);

		m_args << arg;
	}
}

bool QPMenu::run()
{
	if (!hasFlags() || hasFlag("?") || hasFlag("help"))
	{
		return showHelp();
	}

	if(hasFlag("width"))
	{
		const auto width = getFlag("width").toUInt();		
		if (width == 0) return error("invalid width parameter...");		
		m_plot.setFixedWidth(width);
	}

	if(hasFlag("height"))
	{
		const auto height = getFlag("height").toUInt();		
		if (height == 0) return error("invalid height parameter...");		
		m_plot.setFixedHeight(height);
	}
	
	if (hasFlag("color"))
	{
		const auto colarr = getFlag("color").split(":");
		if (colarr.size() != 3) return error("invalid color parameter...");
		m_plot.setColor(QColor(colarr.at(0).toUInt(), colarr.at(1).toUInt(), colarr.at(2).toUInt()));
	}

	if (hasFlag("title"))
	{
		const auto title = getFlag("title");
		if (title.isEmpty()) return error("invalid title parameter...");
		m_plot.setTitle(title);
	}

	if (hasFlag("x-name"))
	{
		const auto name = getFlag("x-name");
		if (name.isEmpty()) return error("invalid x-name parameter...");
		m_plot.setXName(name);
	}

	if (hasFlag("y-name"))
	{
		const auto name = getFlag("y-name");
		if (name.isEmpty()) return error("invalid y-name parameter...");
		m_plot.setYName(name);
	}

	if (hasFlag("dotted"))
	{
		m_plot.setDotted(true);
	}

	if (!hasFlag("input"))
	{
		return error("input must be provided...");
	}

	bool idataok = false;
	m_plot.setData(getMap(getFlag("input"), idataok));
	if (!idataok) return error("input data cannot be loaded...\nPath: '" + QFileInfo(getFlag("input")).absoluteFilePath() + "'");
	
	
	if (hasFlag("debug"))
	{		
		m_plot.show();
	}
	else if (!hasFlag("output"))
	{
		return error("output must be provided...");
	}
	
	if (!saveOutput(getFlag("output")))
		return error("could not save output to: '" + getFlag("output") + "'");

	return true;
}

bool QPMenu::showHelp() const
{
	std::cout << "Ikarus.AI.QuickPlot usage:\n";
	std::cout << "\n";
	std::cout << "Ikarus.AI.QuickPlot.exe [options]\n";
	std::cout << "\n";
	std::cout << "options:\n";
	std::cout << "input [filepath]              loads the given data set (format see below)\n";
	std::cout << "width [pixels]                exported image width\n";
	std::cout << "height [pixels]               exported image height\n";
	//std::cout << "scale [logarithmic/linear]    how to scale the y axis\n";
	std::cout << "dotted                        shows dots for every data point\n";
	std::cout << "color [red:green:blue]        shows the line in the given color (range: 0-255)\n";
	std::cout << "output [filepath]             where to save the created image (png only)\n";
	std::cout << "title [name]                  what the title should be\n";
	std::cout << "x-name [name]                 what name should be drawn to the x-axis\n";
	std::cout << "y-name [name]                 what name should be drawn to the y-axis\n";
	std::cout << "\n";
	std::cout << "input format:\n";
	std::cout << "x1,y1\n";
	std::cout << "x2,y2\n";
	std::cout << "x3,y3\n";
	std::cout << "...\n";

	return false;
}

bool QPMenu::error(const QString& msg) const
{
	std::cout << msg.toStdString() << "\n";
	return false;
}

bool QPMenu::hasFlags() const
{
	return !m_args.isEmpty();
}

bool QPMenu::hasFlag(const QString& flg) const
{
	return m_args.contains(flg, Qt::CaseInsensitive);
}

QString QPMenu::getFlag(const QString& flg) const
{
	const auto index = m_args.indexOf(flg);
	if (index < 0 || index + 1 >= m_args.size())
		return "";
	return m_args.at(index + 1);
}

std::map<double, double> QPMenu::getMap(const QString& path, bool& ok)
{
	QFile fi(path);

	if (!fi.open(QIODevice::ReadOnly)) 
	{
		ok = false;
		return {};
	}

	const auto input = QString(fi.readAll());
	fi.close();

	std::map<double, double> map;
	for (const auto& line : input.split("\n"))
	{
		if (line.isEmpty())
			continue;

		const auto coords = line.split(",");

		if(coords.size() != 2)
			continue;

		map.emplace(coords.at(0).toDouble(), coords.at(1).toDouble());
	}

	ok = true;
	return map;
}

bool QPMenu::saveOutput(const QString& path)
{
	return m_plot.grab().toImage().save(path, "PNG");
}
