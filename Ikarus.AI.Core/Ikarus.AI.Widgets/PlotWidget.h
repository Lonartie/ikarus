#pragma once

#include "stdafx.h"
#include "qcustomplot.h"
#include <QWidget>

namespace Ui { class PlotWidget; }

namespace IK_AI_WIDGETS_NAMESPACE
{
	class IK_AI_WIDGETS_EXPORT PlotWidget: public QWidget
	{
		Q_OBJECT

	public:
		PlotWidget(QWidget* parent = Q_NULLPTR);
		~PlotWidget() = default;

		void setColor(const QColor& color);
		void setTitle(const QString& title);
		void setXName(const QString& name);
		void setYName(const QString& name);

		void setDotted(bool dotted);

		void setData(const std::map<double, double>& dataMap);
		
	private:
		std::shared_ptr<Ui::PlotWidget> m_ui;
		QCustomPlot* m_plt = nullptr;
		QCPScatterStyle m_style;
	};
}