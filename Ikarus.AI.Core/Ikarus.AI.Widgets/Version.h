/************************************************************************/
/*                            IKARUS PROJECT                            */
/************************************************************************/
/*                                                                      */
/* Author      : Leon Gierschner                                        */
/* Date        : 2020.05.24                                             */
/* Description : Versioning file                                        */
/*                                                                      */
/************************************************************************/
#pragma once
#include "stdafx.h"

namespace IK_AI_WIDGETS_NAMESPACE::Version
{
	extern IK_AI_WIDGETS_EXPORT const char* BuildDate;
	extern IK_AI_WIDGETS_EXPORT const char* LastCommitID;
	extern IK_AI_WIDGETS_EXPORT const char* BuildBranch;
	extern IK_AI_WIDGETS_EXPORT const char* AssemblyName;
}                                                                                                    