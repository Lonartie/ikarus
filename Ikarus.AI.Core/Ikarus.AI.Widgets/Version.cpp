#include "stdafx.h"
#include "Version.h"
#include "Ikarus.Common/VersioningSystem.h"

namespace IK_AI_WIDGETS_NAMESPACE::Version
{
	IK_AI_WIDGETS_EXPORT const char* BuildDate = "2006271550";
	IK_AI_WIDGETS_EXPORT const char* LastCommitID = "a098a08e3dde73b5dd80517591b2978d85735f1c";
	IK_AI_WIDGETS_EXPORT const char* BuildBranch = "master";
	IK_AI_WIDGETS_EXPORT const char* AssemblyName = "Ikarus.AI.Widgets";
	
	REGISTER_ASSEMBLY(BuildDate, LastCommitID, BuildBranch, AssemblyName);
}
