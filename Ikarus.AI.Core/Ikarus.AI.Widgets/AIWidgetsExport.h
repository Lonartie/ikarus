#pragma once

#include <QtCore/qglobal.h>

#ifndef BUILD_STATIC
# if defined(IKARUSAIWIDGETS_LIB)
#  define IKARUSAIWIDGETS_EXPORT Q_DECL_EXPORT
#	define QCUSTOMPLOT_COMPILE_LIBRARY
# else
#  define IKARUSAIWIDGETS_EXPORT Q_DECL_IMPORT
#	define QCUSTOMPLOT_USE_LIBRARY
# endif
#else
# define IKARUSAIWIDGETS_EXPORT
#endif

#define IK_AI_WIDGETS_NAMESPACE Ikarus::AI::Widgets
#define IK_AI_WIDGETS_EXPORT IKARUSAIWIDGETS_EXPORT