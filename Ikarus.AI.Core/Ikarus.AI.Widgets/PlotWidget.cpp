#include "stdafx.h"
#include "PlotWidget.h"
#include "qcustomplot.h"
#include "ui_PlotWidget.h"
using namespace IK_AI_WIDGETS_NAMESPACE;

PlotWidget::PlotWidget(QWidget* parent)
	: QWidget(parent)
	, m_ui(std::make_shared<Ui::PlotWidget>())
{
	m_ui->setupUi(this);
	m_plt = m_ui->plot;

	m_ui->title->setVisible(false);
	m_plt->clearGraphs();
	m_plt->addGraph();

	m_style.setShape(QCPScatterStyle::ssNone);
	m_style.setSize(7);
	m_plt->graph(0)->setScatterStyle(m_style);
}

void PlotWidget::setColor(const QColor& color)
{
	QPen pen(color);
	m_style.setPen(pen);
	pen.setWidth(3);
	m_plt->graph(0)->setPen(pen);
	m_plt->graph(0)->setScatterStyle(m_style);
}

void PlotWidget::setTitle(const QString& title)
{
	m_ui->title->setVisible(true);
	m_ui->title->setText(title);
}

void PlotWidget::setXName(const QString& name)
{
	m_plt->xAxis->setLabel(name);
}

void PlotWidget::setYName(const QString& name)
{
	m_plt->yAxis->setLabel(name);
}

void PlotWidget::setDotted(bool dotted)
{
	m_style.setBrush(QBrush(Qt::BrushStyle::SolidPattern));
	m_style.setShape(QCPScatterStyle::ssDisc);
	m_plt->graph(0)->setScatterStyle(m_style);
}

void PlotWidget::setData(const std::map<double, double>& dataMap)
{
	for (const auto& point : dataMap)
	{
		m_plt->graph(0)->addData(point.first, point.second);
	}

	m_plt->replot();
	m_plt->xAxis->rescale();
	m_plt->yAxis->rescale();
}

