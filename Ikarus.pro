TEMPLATE = subdirs                                                             
                                                                               
SUBDIRS += \                                                                  
    Ikarus.Common.Core/Ikarus.Common/Ikarus.Common.pro \
    Ikarus.Common.Core/Ikarus.Common.Tests/Ikarus.Common.Tests.pro \
    Ikarus.Communication.Core/Ikarus.Communication/Ikarus.Communication.pro \
    Ikarus.Communication.Core/Ikarus.Communication.Tests/Ikarus.Communication.Tests.pro \
    Ikarus.OpenCL.Core/Ikarus.OpenCL/Ikarus.OpenCL.pro \
    Ikarus.OpenCL.Core/Ikarus.OpenCL.Tests/Ikarus.OpenCL.Tests.pro \
    Ikarus.AI.Core/Ikarus.AI/Ikarus.AI.pro \
    Ikarus.AI.Core/Ikarus.AI.Tests/Ikarus.AI.Tests.pro                                                                             
                                                                               
CONFIG += ordered                                                              
